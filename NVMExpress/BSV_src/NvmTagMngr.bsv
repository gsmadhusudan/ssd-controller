package NvmTagManager;

interface Ifc_tag_mngr;
   method ActionValue#(Bit#(`TAG_BITS)) get_and_lock_tag (Bit#(`TAG_BITS));
   method ActionValue#(Bit#(`TAG_BITS)) release_tag (Bit#(`TAG_BITS));
endinterface

module mkTagMngr#(Bit#(`TAG_BITS) lo_ind, Bit#(`TAG_BITS) hi_ind) 
   (Ifc_tag_mngr ifc_tag_mngr)
   
   RegFile#(Bit#(`TAG_BITS), Maybe#(Bit#(`TAG_BITS))) rgf_tag_alloc <- (
      mkRegFile(unpack(lo_ind), unpack(hi_ind)));
   FIFO#(Bit#(`TAG_BITS)) ff_idle_tags <- mkSizedFIFO(`TAG_SPACE);
   Reg#(Bool) rg_ready <- mkReg(False);
   Reg#(Bit#(`TAG_BITS)) rg_init_counter <- mkReg(lo_ind);
   
   rule rl_init (!rg_ready)
      if (rg_init_counter == hi_ind + 1)
	 begin
	    rg_ready <= True;
	 end
      else
	 begin
	    ff_idle_tags.enq(rg_init_counter);
	 end
   endrule : rl_init

   method ActionValue#(Bit#(`TAG_BITS)) get_and_lock_tag (Bit#(`TAG_BITS)
      static_tag)
      
   
   endmethod
      
endmodule

endpackage