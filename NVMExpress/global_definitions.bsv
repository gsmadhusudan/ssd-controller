/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: Global Definitions
author name: Maximilian Singh, M.S.Abhishek
email id: maximilian.singh@student.kit.edu

This module holds global definitions which are used by several other modules.
*/

`include "global_parameters"

package global_definitions;

import Vector::*;

typedef struct {
	Bit#(32) cdw15;  // DWord 15
	Bit#(32) cdw14;  // DWord 14
	Bit#(32) cdw13;  // DWord 13
	Bit#(32) cdw12;  // DWord 12
	Bit#(32) cdw11;  // DWord 11
	Bit#(32) cdw10;  // DWord 10
	UInt#(64) prp2;  // DWord 8 & 9
	UInt#(64) prp1;  // DWord 6 & 7
	UInt#(64) mptr;  // DWord 4 & 5
	Bit#(32) reserved2;  // DWord 3
	Bit#(32) reserved1;  // DWord 2
	UInt#(32) nsid;  // DWord 1
	UInt#(16) command_id;
	bit psdt;  // use PRP (0) or SGL (1) for data transfer
	Bit#(5) reserved0;
	Bit#(2) fuse;
	Bit#(8) opcode;
} Nvm_command_type deriving (Bits, Eq);

/*//SGL value passing
//Bit#(128) sgl_entry = {Nvm_command_type.prp1, Nvm_command_type.prp2}

//SGL identifier

typedef struct{
        Bit#(4) zero;
        Vector(4,Bit#(4)) sgl_descriptortype;
}SGL_identifier deriving(Bits,Eq);

//SGL Bit Bucket descriptor
typedef struct{
        Bit#(64) rsv; //reserved
        Bit#(32) length;
        Bit#(24) rsv1; //reserved
        SGL_identifier sgl_identifier;
}SGL_bit_bucket_descriptor deriving(Bits,Eq);

//////rg_SGL descriptor[0]-- SGL_data_block_descriptor
//////rg_SGL descriptor[1]-- SGL_segment_descriptor
//////rg_SGL descriptor[2]-- SGL_last_segment_descriptor

typedef struct{
        Bit#(64) address;
        Bit#(32) length;
        Bit#(24) rsv; //reserved
        SGL_identifier sgl_identifier;
}SGL_descriptor deriving(Bits,Eq);*/



typedef struct {
	CQ_DWord3 dword3;
	CQ_DWord2 dword2;
	Bit#(32) dword1;  // reserved
	Bit#(32) dword0;  // command specific
} Completion_type deriving (Bits, Eq);

typedef struct {
	UInt#(64) address;
	Bit#(`WDC) data;
	UInt#(16) length;
} Nand_write_struct deriving (Bits, Eq);

typedef struct {
	UInt#(64) address;
	Bit#(16) length;
} Nand_request_struct deriving (Bits, Eq);

typedef struct {
	// DWORD - 1
	Bit#(8) reserved1 ;
	Bit#(4) mpsmax ;
	Bit#(4) mpsmin ;
	Bit#(7) reserved2 ;
	Bit#(4) css ;
	Bit#(1) reserved3 ;
		Bit#(4) dstrd ;
	// DWORD - 0
	Bit#(8) to ;
	Bit#(5) reserved4 ;
	Bit#(2) ams ;
	Bit#(1) cqr ;
	Bit#(16) mqes ;
} Controller_capabilities deriving(Bits,Eq) ;

typedef struct {
	// DWORD - 2
	Bit#(16) mjr ;
	Bit#(16) mnr ;
} Version deriving(Bits,Eq) ;

typedef struct {
	Bit#(8) reserved1 ;
	Bit#(4) iocqes ;
	Bit#(4) iosqes ;
	Bit#(2) shn ;
	Bit#(3) ams ;
	Bit#(4) mps ;
	Bit#(3) css ;
	Bit#(3) reserved2 ;
	Bit#(1) en ;
} Controller_configuration deriving(Bits,Eq) ;

typedef struct {
	Bit#(28) reserved ;
	Bit#(2) shst ;
	Bit#(1) cfs ;
	Bit#(1) rdy ;
} Controller_status deriving(Bits,Eq) ;

typedef struct {
	Bit#(4) reserved1 ;
	Bit#(12) acqs ;
	Bit#(4) reserved2 ;
	Bit#(12) asqs ;
} AQA deriving(Bits,Eq) ;

typedef struct {
	Bit#(52) asqb ;
	Bit#(12) reserved ;
} ASQ deriving(Bits,Eq) ;

typedef struct {
	Bit#(52) acqb ;
	Bit#(12) reserved ;		
} ACQ deriving(Bits,Eq) ;

typedef struct {
	Bit#(16) reserved ;
	UInt#(16) sqt ;
} SQTDBL deriving(Bits,Eq) ;

typedef struct {
	Bit#(16) reserved ;
	UInt#(16) cqh ;
} CQHDBL deriving(Bits,Eq) ;

typedef struct {
	Bit#(8) hpw ;
	Bit#(8) mpw ;
	Bit#(8) lpw ;
	Bit#(5) rsv;
	Bit#(3) ab ;
} Arbitration deriving(Bits,Eq) ;

typedef struct {
	Bit#(16) ncqr ;
	Bit#(16) nsqr ;
}  NumberOfQs_Requested deriving(Bits,Eq) ;

typedef struct {
	Bit#(16) ncqa ;
	Bit#(16) nsqa ;
} NumberOfQs_Allocated deriving(Bits,Eq) ;

typedef struct {
	Bit#(8) int_time ;
	Bit#(8) thr ;
} InterruptCoalescing deriving(Bits,Eq) ;

typedef struct {
	bit cd ;
	Bit#(16) iv ;
} InterruptVectorConfiguration deriving(Bits,Eq) ;

typedef struct {
	UInt#(16) sqID ;
	UInt#(16) sqHeadPointer ;
} CQ_DWord2 deriving (Bits,Eq) ;

typedef struct {
	bit status_field_DNR ;
	bit status_field_M   ;
	Bit#(2) status_field_res ;	
	Bit#(3) status_field_SCT ;
	Bit#(8) status_field_SC  ;
	bit phase_tag		 ;
	UInt#(16) commandID		 ;
} CQ_DWord3 deriving (Bits,Eq);

typedef struct {
	Bit#(4) reserved;
	Bit#(4) tag;  // not used
	Bit#(32) address;
	bit dre_realignment;  // not used
	bit end_of_frame;  // not used
	Bit#(6) dre_stream_alignment;  // not used
	bit incr_access_type;  // assumed to be 1'b1
	Bit#(23) length;
} Datamover_cmd_type deriving (Bits,Eq);

typedef enum {
	      READ_NAND,
	      WRITE_NAND,
	      ERASE_NAND
   }Nand_cmd_opcode deriving (Bits,Eq,FShow);


typedef struct {
   Nand_cmd_opcode opcode;
   Bit#(16) length;
   Bit#(TLog#(`ROB_SIZE)) tag;
   UInt#(64) nand_lba;
   UInt#(64) nand_pba;
   Bit#(2)      hybrid;//##
   } Ftl_cmd deriving (Bits,Eq);

typedef struct {
   Bit#(TLog#(`ROB_SIZE)) tag;
   Bit#(meta_data_size) meta_data;
   } Ftl_meta_data#(type meta_data_size);

typedef struct {
   Nand_cmd_opcode opcode;
   Bit#(16) length;
   UInt#(64) nand_pba;
   }Nand_cmd deriving (Bits,Eq);

typedef struct {
   UInt#(64) main_mem_addr;
   Bit#(TLog#(`ROB_SIZE)) tag;
   } Ftl_prp_tag deriving (Bits,Eq);

typedef union tagged {
   struct {
      Bit#(16) rlength;
      } Read;
   struct {
      Bit#(TLog#(`ROB_SIZE)) wtag;
      } Write;
   struct {
      Bit#(TLog#(`ROB_SIZE)) etag;
      } Erase;
   } Finished_cmd deriving (Bits,Eq);
   
typedef struct {
   Bit#(TLog#(`ROB_SIZE)) tag;
   Bit#(1) status;
   } Completed_cmd deriving (Bits,Eq);

typedef struct {
		Bit#(64) prp1_address;
		Bit#(32) offset; // address of the bad block in bb table
		Bit#(32) nbb; // no of bad block to reterive
		Bit#(32) channel_no; // channel no to which the bb request to be sent
		} Bad_block_request deriving (Bits, Eq);

// debug
typedef struct {
		Bit#(32) payload_length;
		UInt#(64) address;
		Bit#(`WDC) data;
		bit valid;
		Bit#(16) tag;
		} Debug_tx_completion deriving (Bits,Eq); 

/* Declare the status of a completion. */
typedef enum {
	      SUCCESS,
	      MAX_Q_SIZE_EXCEEDED,
	      INVALID_QID,
	      ABORT_COMMAND_LIMIT_EXCEEDED,
	      ABORTED,
	      FEATURE_NOT_SAVEABLE,
	      WRITE_FAILED,
	      UNDEFINED
	      } Completion_status_type deriving (Bits, Eq);

interface Ifc_read_data_ddr;
	method bit ready_();
	method Action _valid(bit _is_valid);
	method Action _data_in(Bit#(`WDC) _data);
	method Action _last(bit _is_last);
endinterface

interface Ifc_read_cmd_ddr;
	method bit valid_();
	method Bit#(72) data_();
	method Action _ready(bit _is_ready);
endinterface

interface Ifc_write_data_ddr;
	method bit valid_();
	method Bit#(`WDC) data_out_();
	method bit last_();
	method Action _ready(bit _is_ready);
endinterface

interface Ifc_write_cmd_ddr;
	method bit valid_();
	method Bit#(72) data_();
	method Action _ready(bit _is_ready);
endinterface

interface Ifc_write_sts_ddr;
	method bit ready_();
	method Action _valid(bit _is_valid);
	method Action _data_in(Bit#(8) _data);
endinterface

interface Ifc_nand_flash_model;
	/* .The interface to the NVM Controller */
	method Action _request_data(UInt#(64) _address, UInt#(11) _length);
	method ActionValue#(Bit#(`WDC)) _get_data_();
	method Action _write(UInt#(64) _address, Bit#(`WDC) _data, UInt#(11) _length);
	method Action receive_bb_request(Bad_block_request bb_request);
	method ActionValue#(Bit#(`WDC)) _send_bb;
	method Action _enable(bit _nand_ce_l);
	method bit interrupt_();
	method bit busy_();
	method ActionValue#(bit) write_failed();
endinterface

interface Ifc_main_memory_model;
	method ActionValue#(UInt#(64)) _allocate_prp_();
	method Action _free_prp(UInt#(64) _prp_addr);
	method Action _request_prp(UInt#(64) _prp_addr);
	// TODO make new type
	method ActionValue#(Bit#(`WDC)) get_prp_();

	method Action _put_cmd(Nvm_command_type _cmd, UInt#(64) _sq_addr);
	method Action _put_cmd_wait();
	method Action _request_compl(UInt#(64) _cq_addr);
	method Completion_type get_compl_();

	method Action _nvm_put_data(Bit#(`WDC) _data, UInt#(64) _address,
		UInt#(32) _length);
	method bit nvm_put_data_ready_();
	method Action _nvm_request_data(UInt#(64) _address, UInt#(32) _length);
	method bit nvm_request_data_ready_();
	method ActionValue#(Bit#(`WDC)) _nvm_get_data_();
endinterface

interface Ifc_config;
   method Bit#(64) read_(Bit#(32) address);
   method Action _write(Bit#(32) _address, Bit#(64) _data,
      Bool dword_aligned);
endinterface

interface NvmInterruptSideA_Interface ;
	method bit vector_rdy();
	method Bit#(5) vector_number();
endinterface 

function NvmInterruptSideA_Interface fn_nvmInterruptSideA_ifc(
		Reg#(bit) rg_vector_rdy, Reg#(Bit#(5)) rg_vector_number);
	return (
		interface NvmInterruptSideA_Interface ;
			method bit vector_rdy () ;
				return rg_vector_rdy ;
			endmethod
			method Bit#(5) vector_number () ;
				return rg_vector_number ;
			endmethod
		endinterface
	);
endfunction 

interface Ifc_completion;
	method Action _write(Bit#(`WDC) _data_dword, Bit#(16) _tag);
endinterface

interface NvmTransmitToPCIe_Interface;
	method Bit#(`WDC) data_();
	method UInt#(64) address_();
	method Bit#(16) tag_();
	method Bit#(32) payload_length_();
	method bit write_;
	method bit data_valid_();
	method Action _wait(bit _is_true);
endinterface

interface Ifc_debug;
	method Bit#(5) debug();
	method Completion_status_type debug_compl();
	method Bit#(16) debug_info();
endinterface
 

function  NvmTransmitToPCIe_Interface  fn_nvmTransmitToPCIe_interface(
			Reg#(Bit#(`WDC)) rg_data,
			Reg#(UInt#(64)) rg_address,
			Reg#(Bit#(16)) rg_tag,
			Reg#(bit) rg_write,
			Reg#(Bit#(32)) rg_payload_length,
			Reg#(bit) rg_data_valid,
			Wire#(bit) wr_wait
		);
	return (
		interface NvmTransmitToPCIe_Interface ;
			method Bit#(`WDC) data_();
				return rg_data;
			endmethod
			method UInt#(64) address_();
				return rg_address;
			endmethod
			method Bit#(16) tag_();
				return rg_tag;
			endmethod
			method Bit#(32) payload_length_();
				return rg_payload_length;
			endmethod
			method bit write_();
				return rg_write;
			endmethod
			method bit data_valid_();
				return rg_data_valid;
			endmethod
			method Action _wait (_is_true);
				wr_wait <= _is_true;
			endmethod
		endinterface
	);
endfunction

interface Ifc_nand_flash;
   method ActionValue#(Nand_request_struct) request_address_();
   method ActionValue#(UInt#(64)) request_erase_address_();
   method Action _data_in(Bit#(`WDC) _data);
   method ActionValue#(Nand_write_struct) data_out_();
   method Action _write_status (Bit#(1) write_failed);
   method ActionValue#(Bad_block_request) request_bb_();
   method Action get_bb(Bit#(`WDC) _get_bb);
   method bit _enable();
   method Action _interrupt();
   method Action _busy(bit _is_busy);
endinterface


interface Ifc_nvme_debug;
   method Action _rx_command (Bit#(`WDC) _data, Bit#(16) _tag);
   method Action  _tx_completion (Debug_tx_completion _tx_compl);
endinterface

interface Ifc_ftl_processor_in;
   method Ftl_cmd get_cmd();
   method Action _get_cmd_busy (bit _cmd_busy);
   method UInt#(64) get_prp();
   method Action _get_prp_busy (bit _prp_busy);
   method UInt#(64) get_metadata();
   method Action _get_metadata_busy (bit _meta_data_busy);
endinterface

interface Ifc_ftl_processor_out;
   method Action _put_cmd (Nand_cmd _nand_cmd);
   method Action _put_prp_read (Ftl_prp_tag _prp_tag_read);
   method Action _put_prp_write (Ftl_prp_tag _prp_tag_write);
endinterface

endpackage: global_definitions
