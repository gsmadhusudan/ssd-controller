/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: Testbench for NVM Express Controller
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu

This testbench connects the NVM Express Controller to a PC model and a NAND
flash model.  The main memory of the PC and the NAND-Flash are modeled as BRAMs.
The functionality of the NVM Express Controller is tested by submitting commands
and checking the result.

At the moment only the initialization is done by the test bench.  Refer to the
NVM Express Specification Revision 1.1a (September 23, 2013) section 7.6.1 for
the steps of the initialization.  Parts of this document are also inserted as
comments.

The testbench can be easily extended to test I/O operations by using the already
defined functions and finite state machines.

Often used abbreviations:
	PRP			:	Physical Region Page
	Q			:	Queue
	AQ			:	Admin Queue
	SQ			:	Submission Queue
	CQ			:	Completion Queue
	ASQ			:	Admin Submission Queue
	ACQ			:	Admin Completion Queue
	dw, dWord	:	Double Word
	sqid		:	Submission Queue ID
	cqid		:	completion Queue ID
*/

/*
Memory Map of simulated main memory:
	0x00000|000 - 0x00000|FFF		Admin Submission Queue Base Address
	0x00001|000 - 0x00001|FFF		Admin Completion Queue Base Address
	0x00002|000 - 0x00002|FFF		1. IO Submission Queue Base Address
	0x00003|000 - 0x00003|FFF		1. IO Completion Queue Base Address
	0x00004|000 - 0x00004|FFF		2. IO Submission Queue Base Address
	0x00005|000 - 0x00005|FFF		2. IO Completion Queue Base Address
	0x00006|000 - 0x00006|FFF		3. IO Submission Queue Base Address
	0x00007|000 - 0x00007|FFF		3. IO Completion Queue Base Address
	0x00008|000 - 0x00008|FFF		4. IO Submission Queue Base Address
	0x00009|000 - 0x00009|FFF		4. IO Completion Queue Base Address
	0x0000A|000 - 0x0000A|FFF		5. IO Submission Queue Base Address
	0x0000B|000 - 0x0000B|FFF		5. IO Completion Queue Base Address
	0x0000C|000 - 0x0000F|FFF		Reserved for more queues
	0x00010|000 - 0x0001F|FFF		used for data transmission (PRP)
*/

`include "global_parameters"

/*
Number of pages which are transfered in data test.  Keep in mind, that you have
to adjust the PRP addresses for two page transfers.
*/
`define NR_TEST_PAGES 4 

`define DEBUG 1

package tb_nvm_controller;

import Connectable::*;
import StmtFSM::*;
import Vector::*;

import NvmController::*;
`ifdef ENABLE_DDR_CONNECTION
import ddr_connection::*;
`endif
import global_definitions::*;
import main_memory_model::*;
import nand_flash_model::*;
import FtlProcessor::*;

// states for the main state machine
typedef enum {
	IDLE,
	WAIT_FOR_NVM_READY,
	GET_CAP,
	SET_AQA,
	SET_ASQ,
	SET_ACQ,
	SET_CONTROLLER_CONFIG,
	ENABLE_NVM,
	WAIT_FOR_ENABLED,
	IDENTIFY_CONTROLLER,
	WAIT_FOR_EXEC_CMD,
	IDENTIFY_LNVM,
	WAIT_FOR_IDENTIFY_LNVM,
	GET_FEATURE_LNVM,
	WAIT_FOR_GET_FEATURE_LNVM,
	SET_RESPONSIBILITY_LNVM,
	WAIT_FOR_SET_RESPONSIBILITY_LNVM,
	GET_ACTIVE_NSPACES,
	WAIT_FOR_ACTIVE_NSPACES,
	IDENTIFY_NSPACES,
	WAIT_FOR_IDENTIFY_NSPACE,
	REQUEST_BADBLOCK_LNVM,
	WAIT_FOR_BADBLOCK_LNVM,
	SET_NR_OF_QUEUES,
	WAIT_FOR_NR_OF_QUEUES,
	SET_ARBITRATION_BURST,
	WAIT_FOR_ARBITRATION_BURST,
	CREATE_CQ,
	WAIT_FOR_CREATE_CQ,
	WAIT_FOR_CREATE_CQ_INT,
	WAIT_FOR_CREATE_CQ_DEQ,
	CREATE_SQ,
	WAIT_FOR_CREATE_SQ,
	WAIT_FOR_CREATE_SQ_INT,
	WAIT_FOR_CREATE_SQ_DEQ,
	DATA_WRITE,
	DATA_WRITE_WAIT_FOR_REQUEST,
	DATA_WRITE_WAIT_FOR_INTERRUPT,
	DATA_HY_WRITE,
	DATA_HY_WRITE_WAIT_FOR_REQUEST,
	DATA_HY_WRITE_WAIT_FOR_INTERRUPT,
	DATA_READ,
	DATA_READ_WAIT_FOR_REQUEST,
	DATA_READ_WAIT_FOR_INTERRUPT,
	DATA_HY_READ,
	DATA_HY_READ_WAIT_FOR_REQUEST,
	DATA_HY_READ_WAIT_FOR_INTERRUPT,
	DATA_CHECK,
	DATA_REQUEST_NEXT,
	FINISHED
} Tb_state_type deriving (Bits, Eq);

// states for the submission queue enqueue state machine
typedef enum {
	IDLE,
	START,
	ENQUEUE,
	COMPLETE
} Sq_enq_state_type deriving (Bits, Eq);

// states for the completion queue dequeue state machine
typedef enum {
	IDLE,
	START,
	PROCESS,
	COMPLETE
} Cq_deq_state_type deriving (Bits, Eq);

// states for the execute command sate machine
typedef enum {
	IDLE,
	START,
	ALLOCATE_PRP1,
	ALLOCATE_PRP2,
	ENQ_CMD,
	CHECK_DEQ,
	POLL_DEQ,
	REQ_PRP1,
	SAVE_PRP1,
	FREE_PRP1,
	REQ_PRP2,
	SAVE_PRP2,
	FREE_PRP2
} Exec_cmd_state_type deriving (Bits, Eq);

typedef struct {
	UInt#(32) id;  // Namespace ID (nsid)
	// Namespace Identify structure// TODO make new type
	Bit#(TMul#(TExp#(TAdd#(`MPS, 12)), 8)) data;
} Identify_nspace_type deriving (Bits, Eq);


`ifdef ENABLE_DDR_CONNECTION
interface Ifc_tb_for_nvm_controller;
	interface Ifc_read_data_ddr ifc_read_data_ddr;
	interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	interface Ifc_write_data_ddr ifc_write_data_ddr;
	interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method Action _reg_in(Bit#(32) _data);
	method Bit#(32) reg_out_();
	// method ActionValue#(Bit#(8)) leds_();
endinterface
`endif

(* synthesize *)
(* always_ready *)
(* always_enabled *)

`ifdef ENABLE_DDR_CONNECTION
module mkTb_for_nvm_controller(Ifc_tb_for_nvm_controller);
`else
module mkTb_for_nvm_controller(Empty);
`endif

function UInt#(64) fn_get_sq_addr(UInt#(16) sqid);
	/* Get the base address of the submission queue with id 'sqid'.

	squid == 0 : admin submission queue
	*/
	return ((extend(sqid) * 2) << 12);
endfunction

function UInt#(64) fn_get_cq_addr(UInt#(16) cqid);
	/* Get the base address of the completion queue with id 'cqid'.

	cqid == 0 : admin completion queue
	*/
	return ((extend(cqid) * 2 + 1) << 12);
endfunction


// state register for testbench state machine
Reg#(Tb_state_type) rg_tb_state <- mkReg(IDLE);
// register to hold the capabilities obtained from the controller
Reg#(Controller_capabilities) rg_nvm_cap <- mkRegU();
// register to hold the configuration of the controller
Reg#(Controller_configuration) rg_nvm_config <- mkRegU();

Reg#(AQA) rg_aqa <- mkReg(AQA {  // register for the Admin Queue Attributes
	reserved1: 0,
	acqs: `QSIZE - 1,  // zero based
	reserved2: 0,
	asqs: `QSIZE - 1  // zero based
});

// asqb -> admin submission queue base address
Reg#(ASQ) rg_asq <- mkReg(ASQ {
	asqb: pack(fn_get_sq_addr(0))[63:12],
	reserved: 'd0
});

// acqb -> admin completion queue base address
Reg#(ACQ) rg_acq <- mkReg(ACQ {
	acqb: pack(fn_get_cq_addr(0))[63:12],
	reserved: 'd0
});

// submission queue head pointer
Reg#(UInt#(64)) rg_sq_head[`NO_IO_SQs + 1];
for (Integer i = 0; i < `NO_IO_SQs + 1; i = i + 1)
	rg_sq_head[i] <- mkReg(fn_get_sq_addr(fromInteger(i)));
// submission queue tail pointer
Reg#(UInt#(64)) rg_sq_tail[`NO_IO_SQs + 1];
for (Integer i = 0; i < `NO_IO_SQs + 1; i = i + 1)
	rg_sq_tail[i] <- mkReg(fn_get_sq_addr(fromInteger(i)));
// completion queue head pointer
Reg#(UInt#(64)) rg_cq_head[`NO_IO_CQs + 1];
for (Integer i = 0; i < `NO_IO_CQs + 1; i = i + 1)
	rg_cq_head[i] <- mkReg(fn_get_cq_addr(fromInteger(i)));

/* Registers for Submission Queue Enqueue Finite State Machine */
// state register
Reg#(Sq_enq_state_type) rg_sq_enq_state <- mkReg(IDLE);
// internal dword counter
Reg#(UInt#(5)) rg_sq_enq_dword_count <- mkRegU();  // TODO: parametrize
// holds the command which is to be enqueued
Reg#(Nvm_command_type) rg_sq_enq_command <- mkRegU();
// holds the Submission Queue ID where the command is to be enqueued
Reg#(UInt#(16)) rg_sq_enq_sqid <- mkRegU();

/* Registers for Completion Queue Dequeue Finite State Machine */
// state register
Reg#(Cq_deq_state_type) rg_cq_deq_state <- mkReg(IDLE);
// internal dword counter
Reg#(UInt#(3)) rg_cq_deq_dword_count <- mkRegU();  // TODO: parametrize?
// holds the completion when the FSM is done
Reg#(Maybe#(Completion_type)) rg_cq_deq_completion <-
	mkReg(tagged Invalid);
// holds the Completion Queue ID where the command is dequed
Reg#(UInt#(16)) rg_cq_deq_cqid <- mkRegU();

/*
Please refer to NVM Express Specification Revision 1.1a (September 23, 2013)
section 4.6 for information about the use of phase tags.

 + 1 for admin completion queue
 */
Vector#(TAdd#(`NO_IO_CQs, 1), Reg#(bit)) rg_phase_tags <-
	replicateM(mkReg(1'b1));

// general purpose counter for testbench fsm
Reg#(UInt#(32)) rg_tb_count <- mkReg(0);  // TODO set size

Reg#(UInt#(64)) rg_data_request_addr <- mkReg(0);

/*
Queue Command Identifier, incremented for each submitted command 16 is the
length of the submission queue tail doorbell (SQT) register
// TODO could also be parametrized with `QSIZE...
*/
Vector#(TAdd#(`NO_IO_SQs, 1), Reg#(UInt#(16))) rg_sq_cmd_id <-
	replicateM(mkReg(0));

/* Registers for Execute Command Finite State Machine */
// state register
Reg#(Exec_cmd_state_type) rg_exec_cmd_state <- mkReg(IDLE);
// holds the command to be enqueued
Reg#(Nvm_command_type) rg_exec_cmd_command <- mkRegU();
// holds the Submission Queue ID where the command is to be enqueued
Reg#(UInt#(16)) rg_exec_cmd_sqid <- mkRegU();
// holds the completion of the executed command when the FSM is done
Reg#(Completion_type) rg_exec_cmd_compl <- mkRegU();
// holds the first prp of the executed command when the FSM is done
Reg#(Vector#(TDiv#(TExp#(TAdd#(`MPS, 12)), 4), Bit#(32)))
	rg_exec_cmd_prp1 <- mkRegU();
// holds the second prp of the executed command when the FSM is done
Reg#(Vector#(TDiv#(TExp#(TAdd#(`MPS, 12)), 4), Bit#(32)))
	rg_exec_cmd_prp2 <- mkRegU();
// counter for dwords when getting the PRPs
Reg#(Bit#(TLog#(1024))) rg_exec_counter <- mkReg(0);

/* holds the Identify Controller structure obtained from the controller

TODO make new type
*/
Reg#(Bit#(TMul#(TExp#(TAdd#(`MPS, 12)), 8))) rg_identify_controller <-
	mkRegU();
// holds the Identify Namespace structures obtained from the controller
Vector#(`NO_NSPACES, Reg#(Identify_nspace_type)) rg_identify_nspace <-
	replicateM(mkRegU());

// Counter for simulation clocks
Reg#(int) rg_clock_count <- mkReg(0);

// LED output to display the current state of the testbench state machine
Reg#(Bit#(8)) rg_out_leds <- mkReg('hAA);
// Control data from the processor
Reg#(Bit#(32)) rg_in_reg <- mkReg(0);
// Status data for the processor
Reg#(Bit#(32)) rg_out_reg <- mkReg(0);
// flag for data check
Reg#(Bool) rg_wrong_data <- mkReg(False);


Ifc_Controller lv_nvm_controller <- mkNvmController;
   
`ifdef ENABLE_DDR_CONNECTION
   Ifc_ddr_connection lv_ddr_connection <- mkDdr_connection;
`endif

   
`ifdef ENABLE_DDR_CONNECTION
   let lv_main_memory_model = lv_ddr_connection.ifc_main_memory_model;
   let lv_nand_flash_model = lv_ddr_connection.ifc_nand_flash_model;
`else
   Vector#(`NO_CHANNELS, Ifc_nand_flash_model) lv_nand_flash_model;
   Ifc_main_memory_model lv_main_memory_model <- mkMain_memory_model;
   for (Integer i = 0; i < `NO_CHANNELS; i = i + 1) begin
      lv_nand_flash_model[i] <- mkNand_flash_model;
   end
`endif

Ifc_ftl_processor lv_ftl_proc <- mkFtlProcessor; 
let lv_nvme_ftl_proc_in = lv_nvm_controller.ifc_ftl_processor_in;
let lv_nvme_ftl_proc_out = lv_nvm_controller.ifc_ftl_processor_out;
let lv_ftl_in = lv_ftl_proc.ifc_ftl_in;
let lv_ftl_out = lv_ftl_proc.ifc_ftl_out;

/* renamings for better readability */
let lv_nvm_config_ifc = lv_nvm_controller.ifc_config;
let lv_nvm_completion_ifc = lv_nvm_controller.ifc_completion;
let lv_nvm_to_pci_ifc = lv_nvm_controller.nvmTransmitToPCIe_interface;
let lv_nvm_interrupt_ifc = lv_nvm_controller.nvmInterruptSideA_interface;
let lv_nvm_nand_flash_ifc = lv_nvm_controller.ifc_nand_flash;
`ifdef ENABLE_DDR_CONNECTION
   let lv_read_data_ddr = lv_ddr_connection.ifc_read_data_ddr;
   let lv_read_cmd_ddr = lv_ddr_connection.ifc_read_cmd_ddr;
   let lv_write_data_ddr = lv_ddr_connection.ifc_write_data_ddr;
   let lv_write_cmd_ddr = lv_ddr_connection.ifc_write_cmd_ddr;
   let lv_write_sts_ddr = lv_ddr_connection.ifc_write_sts_ddr;
`endif

function Bool fn_sq_empty(UInt#(16) sqid);
	/* Determine if the submission queue with the id 'sqid' is empty.

	Empty for a queue means that head and tail pointer hold the same vaule.
	*/
	return (rg_sq_head[sqid] == rg_sq_tail[sqid]);
endfunction

function Bool fn_sq_full(UInt#(16) sqid);
	/* Determine if the admin submission queue is full.

	The queue is full if the tail pointer is one entry (of size `SQ_ENTRY_SIZE)
	less than the head pointer or at the corresponding wrap condition.  The wrap
	condition means that the tail pointer points to the end of the queue and the
	head pointer points to the beginning of the queue.
	*/
	Bool lv_normal_full_condition = (rg_sq_tail[sqid] + `SQ_ENTRY_SIZE) ==
		rg_sq_head[sqid];
	Bool lv_wrap_full_condition = (rg_sq_tail[sqid] ==
		(fn_get_sq_addr(sqid) + (`QSIZE - 1) * `SQ_ENTRY_SIZE) &&
		(rg_sq_head[sqid] == fn_get_sq_addr(sqid)));
	return lv_normal_full_condition || lv_wrap_full_condition;
endfunction

function UInt#(16) fn_get_cqid(UInt#(16) sqid);
	/* Get the id of the completion queue which is assigned to the submission
	queue with id 'sqid'.

	For now the each submission queue has a completion queue with a
	corresponding address.
	*/
	return sqid;
endfunction


/* Begin of Submission Queue Enqueue state machine.

This state machine enqueues commands into a submission queue.

Set rg_sq_enq_command and rg_sq_enq_sqid before starting the FSM.
TODO: every command is assumed to be 64 byte in size (`SQ_ENTRY_SIZE)
*/

/* rule rl_sq_enq_idle_state (rg_sq_enq_state == IDLE);

There is no IDLE state because it would do nothing but just wait to be
started.

This pseudo-state is indirectly changed by the rule which starts the fsm.
*/

rule rl_sq_enq_start_state (rg_sq_enq_state == START &&
		rg_cq_deq_state == IDLE);
	/* Proceed only if there is space left in the submission queue. */
	if (`DEBUG == 1) $display("%d: Submission Enqueue state machine started.",
		$stime());
	if (fn_sq_full(rg_sq_enq_sqid)) begin // TODO better error handling
		$display("%d: ERROR: Queue full! This is undefined!", $stime());
		rg_sq_enq_state <= IDLE;
	end else begin
		/* queue not full --> proceed */
		// put the command in the submission queue in the main memory
		lv_main_memory_model._put_cmd(rg_sq_enq_command,
			rg_sq_tail[rg_sq_enq_sqid]);
		rg_sq_enq_state <= ENQUEUE;
	end
endrule

rule rl_sq_enq_enqueue_state (rg_sq_enq_state == ENQUEUE &&
	rg_cq_deq_state == IDLE);
	/* Do all steps necessary for enqueueing the command.

	Update Doorbells etc.
	*/

	/*
	block the execution of the rule until the command is completely saved
	in the main memory model
	*/
	lv_main_memory_model._put_cmd_wait();

	// set command id to next free id;
	UInt#(16) lv_next_sq_cmd_id = 0;
	if (rg_sq_cmd_id[rg_sq_enq_sqid] == `QSIZE - 1)
		lv_next_sq_cmd_id = 0;
	else
		lv_next_sq_cmd_id = rg_sq_cmd_id[rg_sq_enq_sqid] + 1;
	rg_sq_cmd_id[rg_sq_enq_sqid] <= lv_next_sq_cmd_id;

	/*
	update the Submission Queue Tail Doorbell
	use lv_next_sq_cmd_id because the pointer has to be past the new entry
	*/
	lv_nvm_config_ifc._write(
		32'h1000 + (2 * extend(pack(rg_sq_enq_sqid)) * (4 << rg_nvm_cap.dstrd)),
		{32'd0, extend(pack(lv_next_sq_cmd_id))},
		False
	);

	// update sq_tail
	//TODO: can this be calculated from lv_next_sq_cmd_id?
	if (rg_sq_tail[rg_sq_enq_sqid] == fn_get_sq_addr(rg_sq_enq_sqid) +
		(`QSIZE - 1) * `SQ_ENTRY_SIZE) begin
		rg_sq_tail[rg_sq_enq_sqid] <= fn_get_sq_addr(rg_sq_enq_sqid);
	end else begin
		rg_sq_tail[rg_sq_enq_sqid] <= rg_sq_tail[rg_sq_enq_sqid] +
			`SQ_ENTRY_SIZE;
	end

	$display("%d: submissiom enqueue state machine going to idle",$stime()); // TODO to be deleted after debug
	rg_sq_enq_state <= IDLE;
endrule: rl_sq_enq_enqueue_state
/* End of Submission Queue Enqueue state machine. */


/* Begin of the Completion Queue Dequeue state machine.

Get the next completion from completion queue.

Set rg_cq_deq_cqid before starting the state machine.

Result is saved in rg_cq_deq_completion.  It is tagged Invalid if no new
completion is available.  The register should not be used while the state
machine is running.  Every completion is assumed to be 16 bytes as declared
in NVM Express Specification Revision 1.1a (September 23, 2013) section 4.6.
*/

/* rule rl_cq_deq_idle_state (rg_cq_deq_state == IDLE);

There is no IDLE state because it would do nothing but just wait to be
started.

This pseudo-state is indirectly changed by the rule which starts the fsm.
*/

rule rl_cq_deq_start_state (rg_cq_deq_state == START);
	/* Request completion from main memory. */
	if (`DEBUG == 0) $display("%d: Completion Dequeue state machine started for cqid %d.  Expected phase tag: %d",
		$stime(), rg_cq_deq_cqid, rg_phase_tags[rg_cq_deq_cqid]);
	lv_main_memory_model._request_compl(rg_cq_head[rg_cq_deq_cqid]);
	rg_cq_deq_state <= PROCESS;
endrule: rl_cq_deq_start_state

rule rl_cq_deq_process_state (rg_cq_deq_state == PROCESS);
	/* Get and evaluate the completion from main memory. */
	let lv_completion = lv_main_memory_model.get_compl_();
	if (lv_completion.dword3.phase_tag == rg_phase_tags[rg_cq_deq_cqid])
		begin
		/* new completion available */
		$display("%d: Testbench: cq_deq_fsb: valid completion.", $stime());
		// update the Completion Queue head pointer (and phase tag)
		if (rg_cq_head[rg_cq_deq_cqid] ==
				fn_get_cq_addr(rg_cq_deq_cqid) + (`QSIZE - 1) * `CQ_ENTRY_SIZE)
			begin
			// wrap around condition
			rg_cq_head[rg_cq_deq_cqid] <= fn_get_cq_addr(rg_cq_deq_cqid);
			$display("%d: Testbench: cq_deq_fsb: phase tag of cqid %d toggeled.",
				$stime(), rg_cq_deq_cqid);
			rg_phase_tags[rg_cq_deq_cqid] <= ~rg_phase_tags[rg_cq_deq_cqid];
		end else begin
			rg_cq_head[rg_cq_deq_cqid] <= rg_cq_head[rg_cq_deq_cqid] +
			`CQ_ENTRY_SIZE;
		end
		/*
		update Submission Queue head pointer

		TODO is it right that the pointer is relativ to base
		address?
		*/
		rg_sq_head[lv_completion.dword2.sqID] <=
			fn_get_sq_addr(lv_completion.dword2.sqID) +
			extend(lv_completion.dword2.sqHeadPointer) *
			`SQ_ENTRY_SIZE;

		// save completion
		rg_cq_deq_completion <= tagged Valid lv_completion;

		// update the Completion Queue Head Doorbell register
		// TODO is the data calculation right? TODO TODO
		lv_nvm_config_ifc._write(
			'h1000 + (2 * extend(pack(rg_cq_deq_cqid)) + 1) *
			(4 << rg_nvm_cap.dstrd),

			{32'd0, pack((rg_cq_head[rg_cq_deq_cqid] -
			fn_get_cq_addr(rg_cq_deq_cqid)) / `CQ_ENTRY_SIZE)[31:0]},
			False
		);

	end else begin
		/* No new completion available.  Complete with tagged Invalid. */
		$display("%d: Testbench: cq_deq_fsb: invalid completion.  Expected: %d, received: %d.",
			$stime(), rg_phase_tags[rg_cq_deq_cqid],
			lv_completion.dword3.phase_tag);
		rg_cq_deq_completion <= tagged Invalid;
	end
	$display("%d: Testbench: completion queue state machine going to idle",$stime()); // TODO to be deleted after debug
	rg_cq_deq_state <= IDLE;
endrule: rl_cq_deq_process_state
/* End of the Completion Queue Dequeue state machine. */


/* Begin of the Execute Command state machine.

Execute a command.

The command has to be saved in rg_exec_cmd_command before starting the
state machine and the submission queue id has to be saved in
rg_exec_cmd_sqid. The result will be saved in rg_exec_cmd_compl,
rg_exec_cmd_prp1 and rg_exec_cmd_prp2.  Keep in mind that using this state
machine might be a bit less efficient than using the single state machines,
because of extra cycles (register copies and state changes).

The command_id is automically filled in and the prp1 and prp2 entries are
automatically populated with newly allocated pages if requested.  The
calling code has to put a 0 in the prp if it is not used, a 1 if one page is
to be allocated and a 2 if there should be a page list in the prp (not yet
supported).

Note:
	- this state machine uses polling for detecting new queue entries
	- allocated PRPs will be automatically freed after use
*/

/* rule rl_exec_cmd_idle_state (rg_exec_cmd_state == IDLE);

There is no IDLE state because it would do nothing but just wait to be
started.

This pseudo-state is indirectly changed by the rule which starts the fsm.
*/
rule rl_exec_cmd_start_state (rg_exec_cmd_state == START);
	/* Proceed only if there is space left in the submission queue. */
	if (`DEBUG == 1) $display("%d: Execution state machine - START.",
		$stime());
	rg_out_leds <= 32;
	if (fn_sq_full(rg_sq_enq_sqid)) begin  // TODO better error handling
		$display("%d: ERROR: Queue full! This is undefined!", $stime());
		rg_exec_cmd_state <= IDLE;
	end else begin
		/* submission queue not full --> proceed */
		// update command id of the command which is to be enqueued
		rg_exec_cmd_command.command_id <=
			rg_sq_cmd_id[rg_exec_cmd_sqid];
		rg_exec_cmd_state <= ALLOCATE_PRP1;
	end
endrule: rl_exec_cmd_start_state

rule rl_exec_cmd_allocate_prp1_state (rg_exec_cmd_state == ALLOCATE_PRP1);
	/* Allocate PRPs if requested; TODO: support also PRP lists */
	if (`DEBUG == 1) $display("%d: Execution state machine - ALLOCATE_PRP1.",
		$stime());
	rg_out_leds <= 33;
	if (rg_exec_cmd_command.prp1 == 1) begin
		// TODO this offset definition is not really right, see 4.3
		// TODO the use is not really right (bits 0-1 are reserved)
		let lv_prp1 <- lv_main_memory_model._allocate_prp_();
		$display("%d: Testbench: Execution State Machine: prp1 allocated: 0x%x",
			$stime(), lv_prp1);
		rg_exec_cmd_command.prp1 <= lv_prp1;
	end
	rg_exec_cmd_state <= ALLOCATE_PRP2;
endrule: rl_exec_cmd_allocate_prp1_state

rule rl_exec_cmd_allocate_prp2_state (rg_exec_cmd_state == ALLOCATE_PRP2);
	/* Allocate PRP2 if requested; TODO support also PPR lists */
	if (`DEBUG == 1) $display("%d: Execution state machine - ALLOCATE_PRP2.",
		$stime());
	rg_out_leds <= 34;
	if (rg_exec_cmd_command.prp2 == 1) begin
		// TODO this offset definition is not really right, see 4.3
		// TODO the use is not really right (bits 0-1 are reserved)
		let lv_prp2 <- lv_main_memory_model._allocate_prp_();
		rg_exec_cmd_command.prp2 <= lv_prp2;
	end
	rg_exec_cmd_state <= ENQ_CMD;
endrule

rule rl_exec_cmd_enq_cmd_state (rg_exec_cmd_state == ENQ_CMD &&
		rg_sq_enq_state == IDLE);
	/* Use the submission queue enqueue state machine to enqueue the command.

	This rule only fires if the sq_enq state machine is ready to run.
	*/
	if (`DEBUG == 1) $display("%d: Execution state machine - ENQ_CMD.",
		$stime());
	rg_out_leds <= 35;
	// set up and start sq_enq state machine
	rg_sq_enq_command <= rg_exec_cmd_command;
	rg_sq_enq_sqid <= rg_exec_cmd_sqid;
	rg_sq_enq_state <= START;

	rg_exec_cmd_state <= CHECK_DEQ;
endrule: rl_exec_cmd_enq_cmd_state

rule rl_exec_cmd_check_deq_state (rg_exec_cmd_state == CHECK_DEQ &&
		rg_sq_enq_state == IDLE && rg_cq_deq_state == IDLE);
	/* Set up and start the cq_deq state machine to check for the completion.

	This rule only fires if the sq_enq state machine is done and the cq_deq
	state machine is ready to run.
	*/
	if (`DEBUG == 1) $display("%d: Execution state machine - CHECK_DEQ.",
		$stime());
	rg_out_leds <= 36;
	rg_cq_deq_cqid <= fn_get_cqid(rg_exec_cmd_sqid);
	rg_cq_deq_state <= START;

	rg_exec_cmd_state <= POLL_DEQ;
endrule: rl_exec_cmd_check_deq_state

rule rl_exec_cmd_poll_deq_state (rg_exec_cmd_state == POLL_DEQ &&
		rg_cq_deq_state == IDLE);
	/* Rerun the cq_deq state machine until a valid completion is received.

	This rule only fires if the cq_deq state machine is done and thus ready to
	run again.
	*/
	if (`DEBUG == 1) $display("%d: Execution state machine - POLL_DEQ.",
		$stime());
	rg_out_leds <= 37;
	// poll admin completion queue because interrupts are not yet ready
	if (!isValid(rg_cq_deq_completion)) begin
		// try it again until completion is received
	$display("%d: Execution state machine -AFTER POLL_DEQ. going to completion start",
		$stime());
		rg_cq_deq_state <= START;
	end else begin
	$display("%d: Execution state machine -AFTER POLL_DEQ. going to req_prp1",
		$stime());
		rg_exec_cmd_state <= REQ_PRP1;
	end
endrule: rl_exec_cmd_poll_deq_state

rule rl_exec_cmd_req_prp1_state (rg_exec_cmd_state == REQ_PRP1);
	/* Request the first PRP from main memory if it is used by the command. */
	if (`DEBUG == 1) $display("%d: Execution state machine - REQ_PRP1.",
		$stime());
	rg_out_leds <= 38;
	if (rg_exec_cmd_command.prp1 != 0) begin
		// request first PRP
		lv_main_memory_model._request_prp(rg_exec_cmd_command.prp1);
		rg_exec_cmd_state <= SAVE_PRP1;
	end else begin
		rg_exec_cmd_prp1 <= unpack(0);
		rg_exec_cmd_state <= REQ_PRP2;
	end
endrule: rl_exec_cmd_req_prp1_state

rule rl_exec_cmd_save_prp1_state (rg_exec_cmd_state == SAVE_PRP1);
	/* Save the first PRP from the main memory. */
	if (`DEBUG == 1) $display("%d: Execution state machine - SAVE_PRP1.",
		$stime());
	rg_out_leds <= 39;
	let lv_prp_data <-lv_main_memory_model.get_prp_();
	// TODO TODO rg_exec_cmd_prp1[rg_exec_counter] <= lv_prp_data;
	if (rg_exec_counter == fromInteger(valueOf(TSub#(TDiv#(TMul#(4096, 8), `WDC), 1)))) begin
		rg_exec_counter <= 0;
		rg_exec_cmd_state <= FREE_PRP1;
	end else begin
		rg_exec_counter <= rg_exec_counter + 1;
	end
endrule: rl_exec_cmd_save_prp1_state

rule rl_exec_cmd_free_prp1_state (rg_exec_cmd_state == FREE_PRP1);
	/* Free the first PRP for reuse. */
	rg_out_leds <= 40;
	lv_main_memory_model._free_prp(rg_exec_cmd_command.prp1);
	rg_exec_cmd_state <= REQ_PRP2;
endrule: rl_exec_cmd_free_prp1_state

rule rl_exec_cmd_req_prp2_state (rg_exec_cmd_state == REQ_PRP2);
	/* Request the second PRP from main memory if it is used. */
	rg_out_leds <= 41;
	if (rg_exec_cmd_command.prp2 != 0) begin
		// request second PRP
		lv_main_memory_model._request_prp(rg_exec_cmd_command.prp2);
		rg_exec_cmd_state <= SAVE_PRP2;
	end else begin
		$display("%d: rg_exec_cmd_state going to idle",$stime());
		rg_exec_cmd_prp2 <= unpack(0);
		rg_exec_cmd_state <= IDLE;
	end
endrule: rl_exec_cmd_req_prp2_state

rule rl_exec_cmd_save_prp2_state (rg_exec_cmd_state == SAVE_PRP2);
	/* Save the second PRP from the main memory. */
	rg_out_leds <= 42;
	let lv_prp_data <- lv_main_memory_model.get_prp_();
	// TODO TODO rg_exec_cmd_prp2[rg_exec_counter] <= lv_prp_data;
	if (rg_exec_counter == fromInteger(valueOf(TSub#(TDiv#(TMul#(4096, 8), `WDC), 1)))) begin
		rg_exec_counter <= 0;
		rg_exec_cmd_state <= FREE_PRP2;
	end else begin
		rg_exec_counter <= rg_exec_counter + 1;
	end
endrule: rl_exec_cmd_save_prp2_state

rule rl_exec_cmd_free_prp2_state (rg_exec_cmd_state == FREE_PRP2);
	/* Free the second PRP for reuse. */
	rg_out_leds <= 43;
	lv_main_memory_model._free_prp(rg_exec_cmd_command.prp2);
	rg_exec_cmd_state <= IDLE;
endrule: rl_exec_cmd_free_prp2_state
/* End of Execute Command state machine. */


/*
global enable signal which indicates that no Sub-State Machine is running
and thus the main state machine can safely fire.
*/
Bool lv_en = (rg_sq_enq_state == IDLE) && (rg_cq_deq_state == IDLE) &&
	(rg_exec_cmd_state == IDLE);


/* Make connection between lv_nand_flash_model and lv_nvm_nand_flash_ifc */
   for (Integer i = 0; i < `NO_CHANNELS; i = i + 1)
      begin
	 
	 (*conflict_free="rl_connect_nand_request_data,rl_connect_nand_write"*)
	 
	 rule rl_connect_nand_request_data;
	    $display ("%d: Tb_nvm: nand request fired",$stime());
	    let lv_tmp <- lv_nvm_nand_flash_ifc[i].request_address_();
	    lv_nand_flash_model[i]._request_data(
	       truncate(lv_tmp.address),
	       unpack(truncate(lv_tmp.length))
	       );
	 endrule: rl_connect_nand_request_data
	 
	 rule rl_connect_nand_data_in;
	    let lv_tmp <- lv_nand_flash_model[i]._get_data_();
	    lv_nvm_nand_flash_ifc[i]._data_in(lv_tmp);
	 endrule: rl_connect_nand_data_in
	 
	 rule rl_connect_nand_write;
	    $display("%d: Testbench: nand flash write.", $stime());
	    let lv_tmp <- lv_nvm_nand_flash_ifc[i].data_out_();
	    lv_nand_flash_model[i]._write(
	       truncate(lv_tmp.address),
	       lv_tmp.data,
	       truncate(lv_tmp.length)
	       );
	 endrule: rl_connect_nand_write

	 rule rl_connect_badblock;
		let lv_tmp <- lv_nvm_nand_flash_ifc[i].request_bb_();
			lv_nand_flash_model[i].receive_bb_request(lv_tmp);
	 endrule : rl_connect_badblock

	 rule rl_connect_badblock_send;
		let lv_tmp <- lv_nand_flash_model[i]._send_bb;
			lv_nvm_nand_flash_ifc[i].get_bb(lv_tmp);
	 endrule : rl_connect_badblock_send

	 rule rl_connect_enable;
	    lv_nand_flash_model[i]._enable(~lv_nvm_nand_flash_ifc[i]._enable());
	 endrule: rl_connect_enable
      
	 rule rl_connect_interrupt;
	    if (lv_nand_flash_model[i].interrupt_() == 1'b1)
	       lv_nvm_nand_flash_ifc[i]._interrupt();
	 endrule: rl_connect_interrupt
	 
	 rule rl_connect_busy;
	    lv_nvm_nand_flash_ifc[i]._busy(lv_nand_flash_model[i].busy_());
	 endrule: rl_connect_busy

	 rule rl_connect_write_ack;
	    let lv_write_status <- lv_nand_flash_model[i].write_failed();
	    lv_nvm_nand_flash_ifc[i]._write_status(lv_write_status);
	 endrule: rl_connect_write_ack
      end

/* Make connection between ftl_processor and lv_nvm_to_ftl_ifc */
   
   rule rl_connect_ftl_cmd_in;
      let ftl_cmd_in = lv_nvme_ftl_proc_in.get_cmd();
      lv_ftl_in._put_cmd(ftl_cmd_in);
   endrule : rl_connect_ftl_cmd_in
   
   rule rl_connect_ftl_cmd_in_busy;
      let ftl_cmd_in_busy = lv_ftl_in.put_cmd_busy();
      lv_nvme_ftl_proc_in._get_cmd_busy(ftl_cmd_in_busy);
   endrule : rl_connect_ftl_cmd_in_busy

   rule rl_connect_ftl_request_in;
      let ftl_prp_in = lv_nvme_ftl_proc_in.get_prp();
      lv_ftl_in._put_prp(ftl_prp_in);
   endrule : rl_connect_ftl_request_in
   
   rule rl_connect_ftl_request_busy;
      let ftl_prp_in_busy = lv_ftl_in.put_prp_busy();
      lv_nvme_ftl_proc_in._get_prp_busy(ftl_prp_in_busy);
   endrule : rl_connect_ftl_request_busy
  
   rule rl_connect_ftl_metadata_in;
      let ftl_metadata_in = lv_nvme_ftl_proc_in.get_metadata();
      lv_ftl_in._put_metadata(ftl_metadata_in);
   endrule : rl_connect_ftl_metadata_in
   
   rule rl_connect_ftl_metadata_busy;
      let ftl_metadata_busy = lv_ftl_in.put_metadata_busy();
      lv_nvme_ftl_proc_in._get_metadata_busy(ftl_metadata_busy);
   endrule : rl_connect_ftl_metadata_busy
      
   Rules rls_connect_ftl_prp_out_read = emptyRules();
   Rules rls_connect_ftl_prp_out_write = emptyRules();
   
   for (Integer i = 0; i < `NO_CHANNELS; i = i + 1) begin      
      rule rl_connect_ftl_cmd_out;
	 let ftl_cmd_out <- lv_ftl_out[i].get_cmd();
	 lv_nvme_ftl_proc_out[i]._put_cmd(ftl_cmd_out);
	 $display ("%d: Tb_nvm: Enqueued command from ftl to nvm",$stime());
      endrule : rl_connect_ftl_cmd_out
      
      Rules rls_connect_ftl_prp_out_read_t = (rules
	 rule rl_connect_ftl_prp_out_read;
	    $display ("%d: Tb_nvm: Enqueued read prp from ftl to nvm",$stime());
	    let ftl_prp_out_read <- lv_ftl_out[i].get_prp_read();
	    lv_nvme_ftl_proc_out[i]._put_prp_read(ftl_prp_out_read);
	 endrule : rl_connect_ftl_prp_out_read
	 endrules);

      Rules rls_connect_ftl_prp_out_write_t = (rules
	 rule rl_connect_ftl_prp_out_write;
	    $display ("%d: Tb_nvm: Enqueued write prp from ftl to nvm",$stime());
	    let ftl_prp_out_write <- lv_ftl_out[i].get_prp_write();
	    lv_nvme_ftl_proc_out[i]._put_prp_write(ftl_prp_out_write);
	 endrule : rl_connect_ftl_prp_out_write
	 endrules);
      rls_connect_ftl_prp_out_read = rJoinConflictFree(
	 rls_connect_ftl_prp_out_read,rls_connect_ftl_prp_out_read_t);
      rls_connect_ftl_prp_out_write = rJoinConflictFree(
	 rls_connect_ftl_prp_out_write,rls_connect_ftl_prp_out_write_t);
   end
   
   addRules(rls_connect_ftl_prp_out_read);
   addRules(rls_connect_ftl_prp_out_write);

/* Make connection between main_memory_model and lv_nvm_to_pci_ifc and
lv_nvm_completion_ifc. */
Reg#(Bit#(16)) rg_connect_nvm_tag <- mkRegU();

rule rl_connect_nvm_main_mem_ready;
	if (lv_nvm_to_pci_ifc.write_() == 1'b0) begin
		lv_nvm_to_pci_ifc._wait(
			~lv_main_memory_model.nvm_request_data_ready_());
		$display("%d: Testbench: PCIe ready: %d", $stime(),
			lv_main_memory_model.nvm_request_data_ready_());
	end else begin
		lv_nvm_to_pci_ifc._wait(
			~lv_main_memory_model.nvm_put_data_ready_());
	end
endrule: rl_connect_nvm_main_mem_ready

rule rl_connect_nvm_main_mem_read_req(
		lv_nvm_to_pci_ifc.data_valid_() == 1'b1 &&
		lv_nvm_to_pci_ifc.write_() == 1'b0
	);
	lv_main_memory_model._nvm_request_data(
		lv_nvm_to_pci_ifc.address_(),
		unpack(lv_nvm_to_pci_ifc.payload_length_())
	);
	rg_connect_nvm_tag <= lv_nvm_to_pci_ifc.tag_();
endrule: rl_connect_nvm_main_mem_read_req

rule rl_connect_nvm_main_mem_write_req(
		lv_nvm_to_pci_ifc.data_valid_() == 1'b1 &&
		lv_nvm_to_pci_ifc.write_() == 1'b1
	);
	lv_main_memory_model._nvm_put_data(
		lv_nvm_to_pci_ifc.data_(),
		lv_nvm_to_pci_ifc.address_(),
		unpack(extend(lv_nvm_to_pci_ifc.payload_length_()))
	);
endrule: rl_connect_nvm_main_mem_write_req

rule rl_connect_nvm_main_mem_get;
	let lv_data <- lv_main_memory_model._nvm_get_data_();
	//$display("%d: Get data for NVM from Main Memory. 0b%b",
	//	$stime(), lv_data);
	lv_nvm_completion_ifc._write(lv_data, rg_connect_nvm_tag);
endrule: rl_connect_nvm_main_mem_get



rule rl_control_simulation;
	if (rg_clock_count == 250000000) begin
		$display("%d: End of simulation time.", $stime());
		$finish();
	end
	rg_clock_count <= rg_clock_count + 1;
endrule: rl_control_simulation

/* Testbench main state machine

This state machine controlls the main flow through the testbench.  It consists
of the following states:
	IDLE,
	WAIT_FOR_NVM_READY,
	GET_CAP,
	SET_AQA,
	SET_ASQ,
	SET_ACQ,
	SET_CONTROLLER_CONFIG,
	ENABLE_NVM,
	WAIT_FOR_ENABLED,
	IDENTIY_CONTROLLER,
	WAIT_FOR_EXEC_CMD,
	GET_ACTIVE_NSPACES,
	WAIT_FOR_ACTIVE_NSPACES,
	IDENTIFY_NSPACES,
	WAIT_FOR_IDENTIFY_NSPACE,
	SET_NR_OF_QUEUES,
	WAIT_FOR_NR_OF_QUEUES,
	CREATE_CQ,
	WAIT_FOR_CREATE_CQ,
	WAIT_FOR_CQ_INT,
	WATI_FOR_CREATE_CQ_DEQ,
	CREATE_SQ,
	WAIT_FOR_CREATE_SQ,
	WAIT_FOR_CREATE_SQ_INT,
	WAIT_FOR_CREATE_SQ_DEQ
*/

rule rl_tb_idle_state (rg_tb_state == IDLE && lv_en);
	/* Do nothing for now and only change state.

	See NVM Express Specification Revision 1.1a (September 23, 2013) section 4.6
	for details
	*/
	$display("%d: Testbench IDLE", $stime());
	rg_out_leds <= 0;
	/*
	TODO TODO TODO
	make sure that all Completion Queue entries are initialized to zero
	(for phase tag)
	TODO TODO TODO
	*/
	rg_tb_state <= WAIT_FOR_NVM_READY;
endrule: rl_tb_idle_state

rule rl_tb_wait_for_nvm_ready_state (rg_tb_state == WAIT_FOR_NVM_READY &&
		lv_en);
	/* Start initialization of the NVMe Controller

	This is done by testing the controller ready status until it is zero and
	thus the controller can be enabled.
	// TODO: check first if controller is disabled (cc.en == 0) ?

	See NVM Express Specification Revision 1.1a (September 23, 2013) section
	7.6.1 for details
	*/
	$display("%d: Testbench WAIT_FOR_NVM_READY", $stime());
	rg_out_leds <= 1;
	// send read request for controller ready status
	if (lv_nvm_config_ifc.read_(32'h1c)[0] == 0) begin
		/* ready == 0 so we can proceed */
		rg_tb_state <= GET_CAP;
	end
endrule: rl_tb_wait_for_nvm_ready_state

rule rl_tb_get_cap_state (rg_tb_state == GET_CAP && lv_en);
	/* Read the capabilities of the NVM Controller */
	$display("%d: Testbench GET_CAP", $stime());
	rg_out_leds <= 2;
	Controller_capabilities lv_nvm_cap =
		unpack(lv_nvm_config_ifc.read_(32'h0));
	if (lv_nvm_cap != Controller_capabilities {
		reserved1: 0,  // Reserved
		mpsmax: 4'd0,  // max page size
		mpsmin: 4'd0,  // Min page sizw
		reserved2: 0,  // Reserved
		css: 4'b0001,  // only NVM command set supported
		reserved3: 0,  // Reserved
		dstrd: 0,  // DoorBell Stride Value
		to: 8'd10,  // 10 * 500 ms
		reserved4: 0,  // Reserved
		ams: 0,  // only Round Robin supported
		cqr: 1,  // physically contiguous memory for SQ CQs
		mqes: 16'd500  // max Q size = 10
	}) begin
		// TODO better check, use controller configuration
		$display("%d: Unexpected capabilities!", 	$stime());
	end
	rg_nvm_cap <= lv_nvm_cap;
	rg_tb_state <= SET_AQA;
endrule: rl_tb_get_cap_state

rule rl_tb_set_aqa_state (rg_tb_state == SET_AQA && lv_en);
	/* Set the Admin Queue Attributes of the NVM Controller */
	$display("%d: Testbench SET_AQA (Admin Queue Attributes)", $stime());
	rg_out_leds <= 3;
	lv_nvm_config_ifc._write(32'h24, {32'd0, pack(rg_aqa)}, False);
	rg_tb_state <= SET_ASQ;
endrule: rl_tb_set_aqa_state

rule rl_tb_set_asq_state (rg_tb_state == SET_ASQ && lv_en);
	/* Set the Admin Submission Queue Base Address of the NVM Controller */
	$display("%d: Testbench SET_ASQ (Admin Submission Queue Base Address)",
		$stime());
	rg_out_leds <= 4;
	lv_nvm_config_ifc._write(32'h28, pack(rg_asq), False);
	rg_tb_state <= SET_ACQ;
endrule: rl_tb_set_asq_state

rule rl_tb_set_acq_state (rg_tb_state == SET_ACQ && lv_en);
	/* Set the Admin Completion Queue Base Address of the NVM Controller */
	$display("%d: Testbench SET_ACQ (Admin Completion Queue Base Address)",
		$stime());
	rg_out_leds <= 5;
	lv_nvm_config_ifc._write(32'h30, pack(rg_acq), False);
	rg_tb_state <= SET_CONTROLLER_CONFIG;
endrule: rl_tb_set_acq_state

rule rl_tb_set_controller_config_state (rg_tb_state ==
	SET_CONTROLLER_CONFIG && lv_en);
	/* Set the Controller Configuration of the NVM Controller.

	Actually only cc.ams, cc.mps and cc.css should be set, but the NVM
	Controller does not support byte enable (// TODO fix this) so the other
	values are also set and changed later.
	*/
	$display("%d: Testbench SET_CONTROLLER_CONFIG", $stime());
	rg_out_leds <= 6;
	// send write request for controller configuration
	let lv_nvm_config = Controller_configuration {
		reserved1: 0,
		iocqes: 6,  // just a default, later set to real value
		iosqes: 6,  // just a default, later set to real value
		shn: 0,  // just a default (no shutdown)
		ams: 0,  // select round robin arbitration
		mps: `MPS, // memory page size
		css: 0,  // select NVM Command Set
		reserved2: 0,
		en: 0  // do not enable yet
	};
	lv_nvm_config_ifc._write(32'h14, {32'd0, pack(lv_nvm_config)}, False);
	// save configuration for ENABLE_NVM state
	rg_nvm_config <= lv_nvm_config;
	// preenable configuration done -> enable nvm controller
	rg_tb_state <= ENABLE_NVM;
endrule: rl_tb_set_controller_config_state

rule rl_tb_enable_nvm_state (rg_tb_state == ENABLE_NVM && lv_en);
	/* Send command to enable the NVM Controller */
	$display("%d: Testbench ENABLE_NVM", $stime());
	rg_out_leds <= 7;
	// enable controller but leave the rest as configured before
	let lv_nvm_config = rg_nvm_config;
	lv_nvm_config.en = 1;
	// send write request to controller configuration
	lv_nvm_config_ifc._write(32'h14, {32'd0, pack(lv_nvm_config)}, False);
	rg_tb_state <= WAIT_FOR_ENABLED;
endrule: rl_tb_enable_nvm_state

rule rl_tb_wait_for_enabled_state (rg_tb_state == WAIT_FOR_ENABLED && lv_en);
	/* Wait for NVM Controller to be enabled */
	$display("%d: Testbench WAIT_FOR_ENABLED", $stime());
	rg_out_leds <= 8;
	if (lv_nvm_config_ifc.read_(32'h1c)[0] == 1) begin
		/* ready == 1 so it is enabled */
		rg_tb_state <= CREATE_CQ; //changed to CREATE_CQ from IDENTIFY_CONTROLLER TODO again changed to identify_controller
	end
endrule: rl_tb_wait_for_enabled_state

rule rl_tb_identify_controller_state (rg_tb_state == IDENTIFY_CONTROLLER &&
		lv_en);
	$display("%d: Testbench IDENTIFY_CONTROLLER", $stime());
	rg_out_leds <= 9;
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'h06,
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set in exec_cmd fsm
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 1,  // allocate a PRP see exec_cmd fsm
		prp2: 0,  // prp1 is large enough to contain all 4096 bytes
		cdw10: {30'h0, 2'b01},  // request identify controller structure
		cdw11: 0,  // not used for this command
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_exec_cmd_sqid <= 0;  // admin submission queue
	rg_exec_cmd_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= WAIT_FOR_EXEC_CMD;
endrule: rl_tb_identify_controller_state

rule rl_tb_wait_for_exec_cmd_state (rg_tb_state == WAIT_FOR_EXEC_CMD &&
		lv_en);
	/*
	Because of lv_en this rule only fires when the state machine is done.
	*/
	// TODO process command completion (rg_exec_cmd_compl)
	// save data permanently
	$display("%d: Testbench WAIT_FOR_EXEC_CMD", $stime());
	rg_out_leds <= 10;
	// TODO TODO rg_identify_controller <= pack(rg_exec_cmd_prp1);
	rg_tb_state <= GET_ACTIVE_NSPACES;
endrule: rl_tb_wait_for_exec_cmd_state

rule rl_tb_get_active_nspaces_state (rg_tb_state == GET_ACTIVE_NSPACES &&
		lv_en);
	$display("%d: Testbench GET_ACTIVE_NSPACES", $stime());
	rg_out_leds <= 11;
	// send a read command for list of active namespaces
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'h06,
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set in exec_cmd fsm
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 1,  // allocate a PRP, see exec_cmd fsm
		prp2: 0,  // prp1 is large enough to contain all 4096 bytes
		cdw10: {30'h0, 2'b10},  // request list of active namespaces
		cdw11: 0,  // not used for this command
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_exec_cmd_sqid <= 0;  // admin submission queue
	rg_exec_cmd_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= WAIT_FOR_ACTIVE_NSPACES;
endrule: rl_tb_get_active_nspaces_state

rule rl_tb_wait_for_active_nspaces_state (rg_tb_state ==
	WAIT_FOR_ACTIVE_NSPACES && lv_en);
	/* fires when command execution has finished (lv_en) */
	// TODO process completion
	$display("%d: Testbench WAIT_FOR_ACTIVE_NSPACES", $stime());
	rg_out_leds <= 12;
	if (pack(rg_exec_cmd_prp1)[6 * 32 - 1:(6 - 1) * 32] != 32'b0)
		$display("%d: There are more than 5 namespaces active.  This is not really supported only the first five namespaces will be checked.",
			$stime());
	// save the id of the first five active namespaces for further
	// processing
	for (Integer i = 0; i < 5; i = i + 1)
		rg_identify_nspace[i].id <=
			unpack(pack(rg_exec_cmd_prp1)[(i + 1) * 32 - 1:i * 32]);
	// reset counter which is used in the next state
	rg_tb_count <= 0;
	// set up identify namespace command for IDENTIFY_NSPACES state
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'h06,
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set by exec_cmd fsm
		nsid: ?,  // namespace id inserted by INDENTIFY_NSPACES state
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 1,  // allocate a PRP, see exec_cmd fsm
		prp2: 0,  // prp1 is large enough to contain all 4096 bytes
		cdw10: {30'h0, 2'b00},  // identify namespace
		cdw11: 0,  // not used for this command
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_tb_state <= IDENTIFY_NSPACES;
endrule: rl_tb_wait_for_active_nspaces_state

rule rl_tb_identify_nspaces_state (rg_tb_state == IDENTIFY_NSPACES && lv_en);
	$display("%d: Testbench IDENTIFY_NSPACES", $stime());
	rg_out_leds <= 13;
	// send identify namespace command for each active namespace
	// TODO only five are supported yet
	if (rg_identify_nspace[rg_tb_count].id == 0 || rg_tb_count == 5) begin
		// all active namespaces (up to five) identified
		$display("%d: %d active namespaces identified.", $stime(), rg_tb_count);
		rg_tb_count <= 0;
		rg_tb_state <= SET_NR_OF_QUEUES;
	end else begin
		// identify namespace
		rg_exec_cmd_command.nsid <= rg_tb_count;
		rg_exec_cmd_sqid <= 0;  // admin submission queue
		rg_exec_cmd_state <= START;  // start fsm; it is ready because of lv_en
		rg_tb_state <= WAIT_FOR_IDENTIFY_NSPACE;
	end
endrule: rl_tb_identify_nspaces_state

rule rl_tb_wait_for_identify_nspace_state (rg_tb_state ==
	WAIT_FOR_IDENTIFY_NSPACE && lv_en);
	/* Wait until the identify namespace command has finished.

	Because of lv_en this rule only fires when the state machine is done.
	*/
	// TODO process completion
	// save result
	$display("%d: Testbench WAIT_FOR_IDENTIFY_NSPACE", $stime());
	rg_out_leds <= 14;
	// TODO TODO rg_identify_nspace[rg_tb_count].data <=
	//	pack(rg_exec_cmd_prp1);
	// go back to IDENTIFY_NSPACES to identify the next namespace */
	rg_tb_count <= rg_tb_count + 1;
	rg_tb_state <= IDENTIFY_NSPACES;
endrule: rl_tb_wait_for_identify_nspace_state


//lightnvm command
rule rl_tb_identify_lnvm_command_state (rg_tb_state == IDENTIFY_LNVM && lv_en);
	$display("%d: Testbench IDENTIFY_LNVM",$stime());
	//rg_out_leds <= 10;
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'he2,
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set in exec_cmd fsm
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 1,  // allocate a PRP see exec_cmd fsm
		prp2: 0,  // prp1 is large enough to contain all 4096 bytes
		cdw10: {30'h0, 2'b01},  // namespace offset for identify lnvm
		cdw11: 0,  // not used for this command
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_exec_cmd_sqid <= 0;
	rg_exec_cmd_state <= START;
	rg_tb_state <= WAIT_FOR_IDENTIFY_LNVM;
endrule : rl_tb_identify_lnvm_command_state

rule rl_tb_wait_for_identify_lnvm_command_state (rg_tb_state == WAIT_FOR_IDENTIFY_LNVM && lv_en);
	//Because of lv_en this rule only fires when the state machine is done.
	
	// TODO process command completion (rg_exec_cmd_compl)
	// save data permanently
	$display("%d: Testbench: wait for identify lnvm",$stime());// TODO this line should be deleted after checking
	//rg_out_leds <= ;
	rg_tb_state <= GET_FEATURE_LNVM;
endrule : rl_tb_wait_for_identify_lnvm_command_state 


rule rl_tb_get_feature_lnvm_command (rg_tb_state == GET_FEATURE_LNVM && lv_en);
	$display("%d: Testbench: Get feature lnvm",$stime());
	//rg_out_leds <= ;
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'he6,
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set in exec_cmd fsm
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 1,  // prp1 is allocated in execution state machine 
		prp2: 0,  // prp2 is also not needed for this command
		cdw10: 0,  // not used for this command
		cdw11: 0,  // not used for this command
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_exec_cmd_sqid <= 0;
	rg_exec_cmd_state <= START;
	rg_tb_state <= WAIT_FOR_GET_FEATURE_LNVM;
endrule : rl_tb_get_feature_lnvm_command

rule rl_tb_wait_get_feature_lnvm_command (rg_tb_state == WAIT_FOR_GET_FEATURE_LNVM && lv_en);
	//Because of lv_en this rule only fires when the state machine is done.
	
	// TODO process command completion (rg_exec_cmd_compl)
	// save data permanently
	//rg_out_leds <= ;
	$display("%d: Testbench: got completion for Get feature lnvm",$stime());
	rg_tb_state <= SET_RESPONSIBILITY_LNVM; // set responsibility changed to get active namespace
endrule : rl_tb_wait_get_feature_lnvm_command

rule rl_tb_set_responsibility_lnvm (rg_tb_state == SET_RESPONSIBILITY_LNVM && lv_en);
	$display("%d: Testbench: Set responsibility lnvm",$stime());
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'he5,
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set in exec_cmd fsm
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 0,  // prp1 is not used for this command 
		prp2: 0,  // prp2 is not used for this command 
		cdw10: {30'h0, 2'b01},  // used for setting the responsibilty
		cdw11: {30'h0, 2'b10},  // used for setting the responsibility
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_exec_cmd_sqid <= 0;
	rg_exec_cmd_state <= START;
	rg_tb_state <= WAIT_FOR_SET_RESPONSIBILITY_LNVM;
endrule : rl_tb_set_responsibility_lnvm

rule rl_tb_wait_for_set_responsibility_lnvm (rg_tb_state == WAIT_FOR_SET_RESPONSIBILITY_LNVM && lv_en);
	//Because of lv_en this rule only fires when the state machine is done.
	
	// TODO process command completion (rg_exec_cmd_compl)
	// save data permanently
	//rg_out_leds <= ;
	$display("%d: Testbench:got the completion for Set responsibility lnvm",$stime());
	rg_tb_state <= REQUEST_BADBLOCK_LNVM;
endrule : rl_tb_wait_for_set_responsibility_lnvm

rule rl_tb_request_badblock_lnvm (rg_tb_state == REQUEST_BADBLOCK_LNVM && lv_en);
	$display("%d: :/;",$stime());
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'hf2,
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set in exec_cmd fsm
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 1,  // prp1 is not used for this command 
		prp2: 0,  // prp2 is not used for this command 
		cdw10: {30'h0, 2'b01},  // used for setting the responsibilty
		cdw11: {30'h0, 2'b10},  // used for setting the responsibility
		cdw12: 32'd1,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_exec_cmd_sqid <= 0;
	rg_exec_cmd_state <= START;
	rg_tb_state <= WAIT_FOR_BADBLOCK_LNVM;
endrule : rl_tb_request_badblock_lnvm

rule rl_tb_wait_for_badblock_lnvm(rg_tb_state == WAIT_FOR_BADBLOCK_LNVM && lv_en);
	//Because of lv_en this rule only fires when the state machine is done.
	// TODO process command completion (rg_exec_cmd_compl)
	// save data permanently
	//rg_out_leds <= ;
	$display("%d: got badblock info from controller",$stime());//TODO delete this line after debugging
	rg_exec_cmd_sqid <= 0;
	rg_tb_state <= SET_NR_OF_QUEUES;
endrule : rl_tb_wait_for_badblock_lnvm
/*
8. The host should determine the number of I/O Submission Queues and I/O
Completion Queues supported using the Set Features command with the Number of
Queues feature identifier. After determining the number of I/O Queues, the MSI
and/or MSI-X registers should be configured.
*/

rule rl_tb_set_nr_of_queues_state (rg_tb_state == SET_NR_OF_QUEUES && lv_en);
	/* Request the number of queues used by the host. */
	$display("%d: Testbench SET_NR_OF_QUEUES", $stime());
	rg_out_leds <= 15;
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'h09,  // set feature command
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set by exec_cmd fsm
		nsid: 0,  // not namespace specific
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 0,  // no prp required
		prp2: 0,  // no prp required
		cdw10: {1'b0, 23'h0, 8'h07},  // do not save, fid: number of queues
		cdw11: {8'h0, 8'h0, 8'h0, 5'h0,3'b111},  // request 1 cq and 1 sq; 0's based value
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_exec_cmd_sqid <= 0;  // admin submission queue
	rg_exec_cmd_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= WAIT_FOR_NR_OF_QUEUES;
endrule: rl_tb_set_nr_of_queues_state

rule rl_tb_wait_for_number_of_queues_state (rg_tb_state == WAIT_FOR_NR_OF_QUEUES && lv_en);
	/* Wait for completion of number of queues request.

	Because of lv_en this rule only fires when the state machine is done.
	*/
	// TODO make sure that one queue (requested number) is allocated

	/*
	Normally the configuration of MSI / MSI-X interrupts would now take
	place, but we are directly connected to the NVM Controller and thus get
	the interrupts directly because the rest of interrupt logic is done in
	the PCIe Controller.  TODO  There is also no working implementation of
	the interrupt masks in the NVMe Controller TODO or if MSI-X is used no
	implementation of these masks is necessary because the MSI-X masks are
	used.

	So we jump over this step for now.
	*/
	$display("%d: Testbench WAIT_FOR_NR_OF_QUEUES", $stime());
	rg_out_leds <= 16;
	rg_tb_state <= SET_ARBITRATION_BURST;
endrule: rl_tb_wait_for_number_of_queues_state

rule rl_set_arbitration_burst_state (rg_tb_state == SET_ARBITRATION_BURST && lv_en);
// arbitration burst value is set by the host for controller to limit the number of command to be 
// lauched from a queue
	$display("%d: Testbench SET_ARBITRATION_BURST", $stime());
//	rg_out_leds <= 15;
	rg_exec_cmd_command <= Nvm_command_type {
		opcode: 8'h09,  // set feature command
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set by exec_cmd fsm
		nsid: 0,  // not namespace specific
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: 0,  // no prp required
		prp2: 0,  // no prp required
		cdw10: {1'b0, 23'h0, 8'h01},  // do not save, fid: weight for queues
		cdw11: {8'b10, 8'b10, 8'b10, 5'h0,3'b110},  // request 1 cq and 1 sq; 0's based value
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_exec_cmd_sqid <= 0;  // admin submission queue
	rg_exec_cmd_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= WAIT_FOR_ARBITRATION_BURST;
endrule: rl_set_arbitration_burst_state

rule rl_wait_for_arbitration_burst_state (rg_tb_state == WAIT_FOR_ARBITRATION_BURST && lv_en);

	$display("%d: Testbench WAIT_FOR_ARBITRATION_BURST",$stime());
	rg_tb_state <= CREATE_CQ;
	
endrule: rl_wait_for_arbitration_burst_state
/*
9. The host should allocate the appropriate number of I/O Completion Queues
based on the number required for the system configuration and the number
supported by the controller. The I/O Completion Queues are allocated using the
Create I/O Completion Queue command.
*/
rule rl_tb_create_cq_state (rg_tb_state == CREATE_CQ && lv_en);
	/* Create the required number of completion queues (one for now).

	The admin completion queue is not created by a create command, so no
	interrupt vector can be specified.  In the NVM Controller implementation the
	admin completion queue is implicitly connected to vector 0.  So we start
	with vector 1 for queue id 1 (queue id 0 is admin queue).
	TODO is this the intended behaviour?
	*/
	$display("%d: Testbench CREATE_CQ", $stime());
	rg_out_leds <= 17;
	rg_sq_enq_command <= Nvm_command_type {
		opcode: 8'h05,  // create I/O completion queue
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set by sq_enq fsm
		nsid: 0,  // not namespace specific
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		prp1: fn_get_cq_addr(1), // base address of first completion queue
		prp2: 0,  // not used for this command
		// queue size: `QSIZE zero based, queue id: 1
		cdw10: {`QSIZE - 1, 16'h1},
		// interrupt vector 1, interrupt enable, physically contiguous
		cdw11: {16'h1, 14'h0, 1'b1, 1'b1},
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_sq_enq_sqid <= 0;  // admin submission queue
	rg_sq_enq_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= WAIT_FOR_CREATE_CQ;
endrule: rl_tb_create_cq_state

rule rl_tb_wait_for_create_cq_state (rg_tb_state == WAIT_FOR_CREATE_CQ &&
		lv_en);
	/* Wait for enqueue of the create completion I/O queue command.

	Because of lv_en this rule only fires when the state machine is done.
	*/
	$display("%d: Testbench WAIT_FOR_CREATE_CQ", $stime());
	rg_out_leds <= 18;
	rg_tb_state <= WAIT_FOR_CREATE_CQ_INT;
endrule: rl_tb_wait_for_create_cq_state

rule rl_tb_wait_for_create_cq_int_state (rg_tb_state ==
	WAIT_FOR_CREATE_CQ_INT && lv_en);
	/* Wait for the completion interrupt from the NVM Controller */
	$display("%d: Testbench WAIT_FOR_CREATE_CQ_INT", $stime());
	rg_out_leds <= 19;
	if (lv_nvm_interrupt_ifc.vector_rdy() == 1'b1) begin
		if (lv_nvm_interrupt_ifc.vector_number() != 0)
			$display("%d: ERROR: Interrupt expected for vector 0 in WAIT_FOR_CREATE_CQ_INT.",
				$stime());
		// TODO aggregation threshold not used in NVM Controller
		rg_cq_deq_cqid <= 0;  // admin completion queue
		rg_cq_deq_state <= START;  // start fsm; it is ready because of lv_en
		rg_tb_state <= WAIT_FOR_CREATE_CQ_DEQ;
	end
endrule: rl_tb_wait_for_create_cq_int_state

rule rl_tb_wait_for_create_cq_deq_state (rg_tb_state ==
	WAIT_FOR_CREATE_CQ_DEQ && lv_en);
	/* Wait for completion of the dequeue state machine.

	Because of lv_en the rule only fires when the state machine is done.
	*/
	// TODO check completion
	$display("%d: Testbench WAIT_FOR_CREATE_CQ_DEQ", $stime());
	rg_out_leds <= 20;
	rg_tb_state <= CREATE_SQ;
endrule: rl_tb_wait_for_create_cq_deq_state

/*
10. The host should allocate the appropriate number of I/O Submission Queues
based on the number required for the system configuration and the number
supported by the controller. The I/O Submission Queues are allocated using the
Create I/O Submission Queue command.
*/
rule rl_tb_create_sq_state (rg_tb_state == CREATE_SQ && lv_en);
	/* Create the required number of submission queues (one for now). */
	$display("%d: Testbench CREATE_SQ", $stime());
	rg_out_leds <= 21;
	rg_sq_enq_command <= Nvm_command_type {
		opcode: 8'h01,  // create I/O submission queue
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: ?,  // set by sq_enq fsm
		nsid: 0,  // not namespace specific
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer
		/*
		base address of the completion queue corresponding to submission queue
		with id 1
		*/
		prp1: fn_get_sq_addr(1),
		prp2: 0,  // not used for this command
		// queue size: `QSIZE (zero based), queue id: 1
		cdw10: {`QSIZE - 1, 16'h1},
		/*
		completion queue id: 1, reserved, priority: not used, physically
		contiguous
		*/
		cdw11: {pack(fn_get_cqid(1)), 13'h0, 2'b10, 1'b1},
		cdw12: 0,  // not used for this command
		cdw13: 0,  // not used for this command
		cdw14: 0,  // not used for this command
		cdw15: 0  // not used for this command
	};
	rg_sq_enq_sqid <= 0; // admin submission queue
	rg_sq_enq_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= WAIT_FOR_CREATE_SQ;
endrule: rl_tb_create_sq_state

rule rl_tb_wait_for_create_sq_state (rg_tb_state == WAIT_FOR_CREATE_SQ &&
		lv_en);
	/* Wait for enqueue of the create I/O submission queue command.

	Because of lv_en the rule only fires when the state machine is done.
	*/
	$display("%d: Testbench WAIT_FOR_CREATE_SQ", $stime());
	rg_out_leds <= 22;
	rg_tb_state <= WAIT_FOR_CREATE_SQ_INT;
endrule: rl_tb_wait_for_create_sq_state

rule rl_tb_wait_for_create_sq_int_state (rg_tb_state ==
	WAIT_FOR_CREATE_SQ_INT && lv_en);
	/* Wait for the completion interrupt from the NVM Controller */
	$display("%d: Testbench WAIT_FOR_CREATE_SQ_INT", $stime());
	rg_out_leds <= 23;
	if (lv_nvm_interrupt_ifc.vector_rdy() == 1'b1) begin
		if (lv_nvm_interrupt_ifc.vector_number() != 0)
			$display("%d: ERROR: Interrupt expected for vector 0 in WAIT_FOR_CREATE_SQ_INT.",
				$stime());
		// TODO aggregation threshold not used in NVM Controller
		rg_cq_deq_cqid <= 0;  // admin completion queue
		rg_cq_deq_state <= START;  // start fsm; it is ready because of lv_en
		rg_tb_state <= WAIT_FOR_CREATE_SQ_DEQ;
	end
endrule: rl_tb_wait_for_create_sq_int_state

rule rl_tb_wait_for_create_sq_deq_state (rg_tb_state ==
	WAIT_FOR_CREATE_SQ_DEQ && lv_en);
	/* Wait for completion of the dequeue state machine.

	Because of lv_en the rule only fires when the state machine is done.
	*/
	// TODO check completion
	$display("%d: Testbench WAIT_FOR_CREATE_SQ_DEQ", $stime());
	rg_out_leds <= 24;
	$display("%d: Initialization finished!", $stime());
	rg_tb_state <= DATA_HY_WRITE;
endrule: rl_tb_wait_for_create_sq_deq_state


rule rl_tb_data_write_state (rg_tb_state == DATA_WRITE && lv_en);
	$display("%d: Testbench DATA_COPY WRITE", $stime());
	rg_out_leds <= 25;
	rg_sq_enq_command <= Nvm_command_type {
		opcode: 8'h01,  // Read: 8'h02, Write: 8'h01
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: 1000,
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer

/*
		prp1: 64'h00020000,
		prp2: 64'h0002F000,
		cdw10: {32'h00030000},  // LSBs of start address
		cdw11: {32'h0},  // MSBs of start address
		cdw12: {16'h0, 16'd14},  // not used for this command, NLB
*/

	        // prp1 : 64'h0,
		prp1: 64'h00021000,
		prp2: 64'h00020000,
		cdw10: {32'h00004000},  // LSBs of start address
		cdw11: {32'h0},  // MSBs of start address
		cdw12: {16'h0, `NR_TEST_PAGES - 1},  // not used for this command, NLB

		cdw13: 32'h00004001,  // not used for this command
		cdw14: 32'h00020000, // LSB of PBA
		cdw15: 32'h0 // MSB of PBA
	};
	rg_sq_enq_sqid <= 1;  // first IO submission queue
	rg_sq_enq_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= DATA_WRITE_WAIT_FOR_REQUEST;
endrule: rl_tb_data_write_state

rule rl_tb_data_write_wait_for_request_state (rg_tb_state ==
		DATA_WRITE_WAIT_FOR_REQUEST && lv_en);
	$display("%d: Testbench WAIT_FOR_DATA_WRITE_REQUEST", $stime());
	rg_out_leds <= 26;
	rg_tb_state <= DATA_WRITE_WAIT_FOR_INTERRUPT;
endrule: rl_tb_data_write_wait_for_request_state

rule rl_tb_data_write_wait_for_interrupt_state (rg_tb_state ==
		DATA_WRITE_WAIT_FOR_INTERRUPT && lv_en);
	/*
	Because of lv_en this rule only fires when the state machine is done.
	*/
	// TODO process command completion (rg_exec_cmd_compl)
	$display("%d: Testbench WAIT_FOR_DATA_WRITE_REQUEST_INT", $stime());
	rg_out_leds <= 27;
	if (lv_nvm_interrupt_ifc.vector_rdy() == 1'b1)
		if (lv_nvm_interrupt_ifc.vector_number() == 1)
			rg_tb_state <= DATA_READ;
endrule: rl_tb_data_write_wait_for_interrupt_state


rule rl_tb_hybrid_write_state (rg_tb_state == DATA_HY_WRITE && lv_en);
	$display("%d: Testbench Hybrid data write", $stime());
	rg_sq_enq_command <= Nvm_command_type {
		opcode: 8'h81, // hybrid write
		fuse:0,
		reserved0: 0,
		psdt: 0, // use of prps in data transfer
		command_id: 1000,
		nsid: 0, // namespace id only for namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0, // no metadata for this transfer
		prp1: 64'h00021000,
		prp2: 64'h00020000,
		cdw10: {32'h00004000}, // LSBs of start address
		cdw11: {32'h0}, // MSB of start address
		cdw12: {16'h0, `NR_TEST_PAGES - 1}, // not used for this command, NLB

		cdw13: 32'h00004001, // not used for this command
		cdw14: 32'h00020000, // LSB of PBA
		cdw15: 32'h0 // MSB of PBA
	};
	rg_sq_enq_sqid <= 1; //first IO submission queue
	rg_sq_enq_state <= START; // start fsm; it is ready because of kv_en
	rg_tb_state <= DATA_HY_WRITE_WAIT_FOR_REQUEST;
endrule: rl_tb_hybrid_write_state

rule rl_tb_hybrid_wait_for_request_state (rg_tb_state == DATA_HY_WRITE_WAIT_FOR_REQUEST && lv_en);
	
	$display("%d: Testbench WAIT_FOR_HY_DATA_WRITE_REQUEST", $stime());
	rg_tb_state <= DATA_HY_WRITE_WAIT_FOR_INTERRUPT;
endrule: rl_tb_hybrid_wait_for_request_state

rule rl_tb_hybrid_data_write_wait_for_interrupt_state (rg_tb_state == DATA_HY_WRITE_WAIT_FOR_INTERRUPT && lv_en);
	$display("%d: Testbench WAIT_FOR_HY_WRITE_INTERRUPT", $stime());
	if(lv_nvm_interrupt_ifc.vector_number() == 1)
		rg_tb_state <= DATA_HY_READ;
endrule: rl_tb_hybrid_data_write_wait_for_interrupt_state


/*rule rl_waveform;
	$dumpvars();
endrule: rl_waveform
*/


rule rl_tb_data_read_state (rg_tb_state == DATA_READ && lv_en);
	$display("%d: Testbench DATA_COPY READ", $stime());
	rg_out_leds <= 28;
	rg_sq_enq_command <= Nvm_command_type {
		opcode: 8'h02,  // Read: 8'h02, Write: 8'h01
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: 1001,
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer

	   /* 32 bit data width 
		prp1: 64'h00040000,
		prp2: 64'h000200F8,
		cdw10: {32'h00000000},  // LSBs of start address
		cdw11: {32'h0},  // MSBs of start address
		cdw12: {16'h0, `NR_TEST_PAGES - 1},  // not used for this command, NLB
	   */
	   
	  
	   /* 128 bit data width */
		prp1: 64'h00223000,
	        //prp1 : 64'h0,
		prp2: 64'h00222000,
		cdw10: {32'h00004000},  // LSBs of start address
		cdw11: {32'h0},  // MSBs of start address
		cdw12: {16'h0, `NR_TEST_PAGES - 1},  // not used for this command, NLB
	   
		cdw13: 32'h00004001,  // not used for this command
		cdw14: 32'h00020000, // LSB of pba
		cdw15: 32'h0 // MSB of pba
	};
	rg_sq_enq_sqid <= 1;  // first IO submission queue
	rg_sq_enq_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= DATA_READ_WAIT_FOR_REQUEST;
endrule: rl_tb_data_read_state

rule rl_tb_data_read_wait_for_request_state (rg_tb_state ==
		DATA_READ_WAIT_FOR_REQUEST && lv_en);
	$display("%d: Testbench WAIT_FOR_DATA_READ_REQUEST", $stime());
	rg_out_leds <= 29;
	rg_tb_state <= DATA_READ_WAIT_FOR_INTERRUPT;
endrule: rl_tb_data_read_wait_for_request_state

rule rl_tb_data_read_wait_for_interrupt_state (rg_tb_state ==
		DATA_READ_WAIT_FOR_INTERRUPT && lv_en);
	/*
	Because of lv_en this rule only fires when the state machine is done.
	*/
	// TODO process command completion (rg_exec_cmd_compl)
	$display("%d: Testbench WAIT_FOR_DATA_READ_REQUEST_INT", $stime());
	rg_out_leds <= 30;
	if (lv_nvm_interrupt_ifc.vector_rdy() == 1'b1) begin
		if (lv_nvm_interrupt_ifc.vector_number() == 1) begin
			lv_main_memory_model._request_prp(64'h00223000);
			rg_data_request_addr <= 64'h00223000;
			//lv_main_memory_model._request_prp(64'h00040000);
			//rg_data_request_addr <= 64'h00040000;
			rg_tb_state <= DATA_CHECK;
		end
	end
endrule: rl_tb_data_read_wait_for_interrupt_state

rule rl_tb_hybrid_data_read_state (rg_tb_state == DATA_HY_READ && lv_en);
	$display("%d: Testbench HYBRID DATA_COPY READ", $stime());
	rg_out_leds <= 28;
	rg_sq_enq_command <= Nvm_command_type {
		opcode: 8'h82,  // Read: 8'h02, Write: 8'h01
		fuse: 0,
		reserved0: 0,
		psdt: 0,  // use prps for data transfer
		command_id: 1001,
		nsid: 0,  // namespace id only used for Namespace data structure
		reserved1: 0,
		reserved2: 0,
		mptr: 0,  // no metadata for this transfer

	   /* 32 bit data width 
		prp1: 64'h00040000,
		prp2: 64'h000200F8,
		cdw10: {32'h00000000},  // LSBs of start address
		cdw11: {32'h0},  // MSBs of start address
		cdw12: {16'h0, `NR_TEST_PAGES - 1},  // not used for this command, NLB
	   */
	   
	  
	   /* 128 bit data width */
		prp1: 64'h00223000,
	        //prp1 : 64'h0,
		prp2: 64'h00222000,
		cdw10: {32'h00004000},  // LSBs of start address
		cdw11: {32'h0},  // MSBs of start address
		cdw12: {16'h0, `NR_TEST_PAGES - 1},  // not used for this command, NLB
	   
		cdw13: 32'h00004001,  // not used for this command
		cdw14: 32'h00020000, // LSB of pba
		cdw15: 32'h0 // MSB of pba
	};
	rg_sq_enq_sqid <= 1;  // first IO submission queue
	rg_sq_enq_state <= START;  // start fsm; it is ready because of lv_en
	rg_tb_state <= DATA_HY_READ_WAIT_FOR_REQUEST;
endrule: rl_tb_hybrid_data_read_state

rule rl_tb_hybrid_data_read_wait_for_request_state (rg_tb_state ==
		DATA_HY_READ_WAIT_FOR_REQUEST && lv_en);
	$display("%d: Testbench WAIT_FOR_DATA_HY_READ_REQUEST", $stime());
	rg_out_leds <= 29;
	rg_tb_state <= DATA_HY_READ_WAIT_FOR_INTERRUPT;
endrule: rl_tb_hybrid_data_read_wait_for_request_state

rule rl_tb_hybrid_data_read_wait_for_interrupt_state (rg_tb_state ==
		DATA_HY_READ_WAIT_FOR_INTERRUPT && lv_en);
	/*
	Because of lv_en this rule only fires when the state machine is done.
	*/
	// TODO process command completion (rg_exec_cmd_compl)
	$display("%d: Testbench WAIT_FOR_DATA_HY_READ_REQUEST_INT", $stime());
	rg_out_leds <= 30;
	if (lv_nvm_interrupt_ifc.vector_rdy() == 1'b1) begin
		if (lv_nvm_interrupt_ifc.vector_number() == 1) begin
			lv_main_memory_model._request_prp(64'h00223000);
			rg_data_request_addr <= 64'h00223000;
			//lv_main_memory_model._request_prp(64'h00040000);
			//rg_data_request_addr <= 64'h00040000;
			rg_tb_state <= DATA_CHECK;
		end
	end
endrule: rl_tb_hybrid_data_read_wait_for_interrupt_state


rule rl_tb_data_check_state (rg_tb_state == DATA_CHECK && lv_en);
	rg_out_leds <= 31;
	if (rg_tb_count == 0)
		$display("%d: Testbench DATA_CHECK", $stime());
	let lv_prp_data <- lv_main_memory_model.get_prp_();
	if (lv_prp_data == extend(pack(rg_tb_count))) begin
		$display("%d: Testbench: Correct Data: 0x%x - 0x%x", $stime(), lv_prp_data, rg_tb_count);
	end else begin
		$display("%d: Testbench: Wrong Data: 0x%x - 0x%x",
			$stime(), lv_prp_data, rg_tb_count);
		rg_wrong_data <= True;
	end
	if (rg_tb_count == fromInteger(valueOf(TMul#(TDiv#(TMul#(4096, 8), `WDC), `NR_TEST_PAGES))) - 1) begin
		rg_tb_count <= 0;
		$display("%d: Testbench: Data Test finished.", $stime());
		rg_tb_state <= FINISHED;
	end else if ((pack(rg_tb_count) & fromInteger(valueOf(TSub#(TDiv#(TMul#(4096, 8), `WDC), 1)))) == fromInteger(valueOf(TSub#(TDiv#(TMul#(4096, 8), `WDC), 1)))) begin
		$display("%d: Testbench: Switch to DATA_REQUEST_NEXT.", $stime());
		rg_tb_count <= rg_tb_count + 1;
		rg_tb_state <= DATA_REQUEST_NEXT;
	end else begin
		rg_tb_count <= rg_tb_count + 1;
	end
endrule: rl_tb_data_check_state

rule rl_tb_data_request_next_state (rg_tb_state == DATA_REQUEST_NEXT  &&
		lv_en);
	rg_out_leds <= 32;
	$display("%d: Testbench: Requesting data for check from 0x%x", $stime(),
		rg_data_request_addr + 'h1000);
	lv_main_memory_model._request_prp(rg_data_request_addr + 'h1000);
	rg_data_request_addr <= rg_data_request_addr + 'h1000;
	rg_tb_state <= DATA_CHECK;
endrule: rl_tb_data_request_next_state

/*
11. If the host desires asynchronous notification of error or health events, the
host should submit an appropriate number of Asynchronous Event Request commands.
This step may be done at any point after the controller signals it is ready
(i.e., CSTS.RDY is set to ‘1’).

TODO this is not used at the moment so we jump over this step for now.
*/

rule rl_tb_finished (rg_tb_state == FINISHED  && lv_en);
	if (rg_wrong_data) begin
		$display("End of Testbench State Machine.  Wrong data received.");
		rg_out_leds <= 33;
	end else begin
		$display("End of Testbench State Machine.  All data correct.");
		rg_out_leds <= 34;
	end
	$finish();
endrule: rl_tb_finished




Reg#(UInt#(32)) rg_led_counter <- mkReg(0);
Reg#(UInt#(8)) rg_valid_counter <- mkReg(0);
rule rl_valid_count (lv_nvm_interrupt_ifc.vector_rdy() == 1'b1);
	$display("%d: valid counter: ########################################## %d",
		$stime(), rg_valid_counter + 1);
	rg_valid_counter <= rg_valid_counter + 1;
endrule: rl_valid_count

`ifdef ENABLE_DDR_CONNECTION
interface Ifc_read_data_ddr ifc_read_data_ddr;
	method bit ready_();
		return lv_read_data_ddr.ready_();
	endmethod: ready_

	method Action _valid(bit _is_valid);
		lv_read_data_ddr._valid(_is_valid);
	endmethod: _valid

	method Action _data_in(Bit#(32) _data);
		lv_read_data_ddr._data_in(_data);
	endmethod: _data_in

	method Action _last(bit _is_last);
		lv_read_data_ddr._last(_is_last);
	endmethod: _last
endinterface: ifc_read_data_ddr


interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	method bit valid_();
		return lv_read_cmd_ddr.valid_();
	endmethod: valid_

	method Bit#(72) data_();
		return lv_read_cmd_ddr.data_();
	endmethod: data_

	method Action _ready(bit _is_ready);
		lv_read_cmd_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_read_cmd_ddr


interface Ifc_write_data_ddr ifc_write_data_ddr;
	method bit valid_();
		return lv_write_data_ddr.valid_();
	endmethod: valid_

	method Bit#(32) data_out_();
		return lv_write_data_ddr.data_out_();
	endmethod: data_out_

	method bit last_();
		return lv_write_data_ddr.last_();
	endmethod: last_

	method Action _ready(bit _is_ready);
		lv_write_data_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_write_data_ddr


interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	method bit valid_();
		return lv_write_cmd_ddr.valid_();
	endmethod: valid_

	method Bit#(72) data_();
		return lv_write_cmd_ddr.data_();
	endmethod: data_

	method Action _ready(bit _is_ready);
		lv_write_cmd_ddr._ready(_is_ready);
	endmethod: _ready
endinterface: ifc_write_cmd_ddr


interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method bit ready_();
		return lv_write_sts_ddr.ready_();
	endmethod: ready_

	method Action _valid(bit _is_valid);
		lv_write_sts_ddr._valid(_is_valid);
	endmethod: _valid

	method Action _data_in(Bit#(8) _data);
		lv_write_sts_ddr._data_in(_data);
	endmethod: _data_in
endinterface: ifc_write_sts_ddr


method Action _reg_in(Bit#(32) _data);
	/* Get control data from processor. */
	rg_in_reg <= _data;
endmethod: _reg_in

method Bit#(32) reg_out_();
	/* Return status data to processor. */
	return rg_out_reg;
endmethod: reg_out_

/*
method ActionValue#(Bit#(8)) leds_();
	// Drive Board-LEDs.
	if (rg_led_counter == 100000000)
		rg_led_counter <= 0;
	else
		rg_led_counter <= rg_led_counter + 1;
	if (rg_led_counter < 30000000)
		return pack(rg_valid_counter);
	else
		return rg_out_leds;
endmethod: leds_
*/
`endif
endmodule: mkTb_for_nvm_controller
endpackage: tb_nvm_controller
