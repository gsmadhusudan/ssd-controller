/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: DDR Connection
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu

This module provides the NAND Flash Model and the Main Memory Model for tests on
the Zynq.  The data of the NAND and the Main Memory is saved in the DDR of the
Board.  This module is the interface to the Xilinx AXI Datamover IP.  The AXI
Datamover is used to read and write data to the DDR.
*/

`include "global_parameters"

package ddr_connection;

/* import bluespec libraries */
import DReg::*;
import BRAM::*;
import Vector::*;
import StmtFSM::*;
import Arbiter::*;

/* import user libraries */
import global_definitions::*;

/*
This type contains the information which is sent to the command ports (s2mm and
mm2s) of the Xilinx AXI Datamover IP to start the transmission.
*/
typedef struct {
	UInt#(32) address;
	UInt#(23) length;
} Ddr_info deriving (Bits, Eq);

interface Ifc_nand_flash_model_32;
	/* .The interface to the NVM Controller */
	method Action _request_data(UInt#(64) _address, UInt#(11) _length);
	method ActionValue#(Bit#(32)) _get_data_();
	method Action _write(UInt#(64) _address, Bit#(32) _data, UInt#(11) _length);
	method Action _enable(bit _nand_ce_l);
	method bit interrupt_();
	method bit busy_();
endinterface

interface Ifc_main_memory_model_32;
	method ActionValue#(UInt#(64)) _allocate_prp_();
	method Action _free_prp(UInt#(64) _prp_addr);
	method Action _request_prp(UInt#(64) _prp_addr);
	// TODO make new type
	method ActionValue#(Bit#(32)) get_prp_();

	method Action _put_cmd(Nvm_command_type _cmd, UInt#(64) _sq_addr);
	method Action _put_cmd_wait();
	method Action _request_compl(UInt#(64) _cq_addr);
	method Completion_type get_compl_();

	method Action _nvm_put_data(Bit#(32) _data, UInt#(64) _address,
		UInt#(32) _length);
	method bit nvm_put_data_ready_();
	method Action _nvm_request_data(UInt#(64) _address, UInt#(32) _length);
	method bit nvm_request_data_ready_();
	method ActionValue#(Bit#(32)) _nvm_get_data_();
endinterface

/* interface declaration */
interface Ifc_ddr_connection;
	interface Ifc_nand_flash_model_32 ifc_nand_flash_model;
	interface Ifc_main_memory_model_32 ifc_main_memory_model;
	interface Ifc_read_data_ddr ifc_read_data_ddr;
	interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	interface Ifc_write_data_ddr ifc_write_data_ddr;
	interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	interface Ifc_write_sts_ddr ifc_write_sts_ddr;
endinterface

/*
The interface to the Xilinx AXI Datamover IP should be always enabled and always
ready.
*/
(* always_enabled = "ifc_read_data_ddr.ready_, ifc_read_data_ddr._valid, ifc_read_data_ddr._data_in, ifc_read_data_ddr._last, ifc_read_cmd_ddr.valid_, ifc_read_cmd_ddr.data_, ifc_read_cmd_ddr._ready, ifc_write_data_ddr.valid_, ifc_write_data_ddr.data_out_, ifc_write_data_ddr.last_, ifc_write_data_ddr._ready, ifc_write_cmd_ddr.valid_, ifc_write_cmd_ddr.data_, ifc_write_cmd_ddr._ready, ifc_write_sts_ddr._data_in, ifc_write_sts_ddr._valid, ifc_write_sts_ddr.ready_ " *)
(* always_ready = "ifc_read_data_ddr.ready_, ifc_read_data_ddr._valid, ifc_read_data_ddr._data_in, ifc_read_data_ddr._last, ifc_read_cmd_ddr.valid_, ifc_read_cmd_ddr.data_, ifc_read_cmd_ddr._ready, ifc_write_data_ddr.valid_, ifc_write_data_ddr.data_out_, ifc_write_data_ddr.last_, ifc_write_data_ddr._ready, ifc_write_cmd_ddr.valid_, ifc_write_cmd_ddr.data_, ifc_write_cmd_ddr._ready, ifc_write_sts_ddr._data_in, ifc_write_sts_ddr._valid, ifc_write_sts_ddr.ready_ " *)

(* synthesize *)
//(* always_ready *)
//(* always_enabled *)
module mkDdr_connection(Ifc_ddr_connection);

/*
The arbiter are used to arbitrate between the requests of the methods of the
main memory interface and the nand flash interface access the DDR.
*/
Arbiter_IFC#(4) rr_ddr_read <- mkStickyArbiter();
Arbiter_IFC#(4) rr_ddr_write <- mkStickyArbiter();

/* Wires of the Read Data interface of the Xilinx AXI Datamover IP */
Wire#(bit) wr_in_read_data_ddr_valid <- mkWire();
Wire#(Bit#(32)) wr_in_read_data_ddr_data <- mkWire();
Wire#(bit) wr_in_read_data_ddr_last <- mkWire();
Wire#(bit) dwr_out_read_data_ready <- mkDWire(1'b0);

/* Wires of the Read Command interface of the Xilinx AXI Datamover IP */
Reg#(bit) rg_out_read_cmd_ddr_valid <- mkReg(0);
Wire#(bit) wr_in_read_cmd_ddr_ready <- mkWire();
Reg#(UInt#(32)) rg_out_ddr_read_addr <- mkReg(0);
Reg#(UInt#(23)) rg_out_ddr_read_length <- mkReg(0);

/* Wires of the Write Data interface of the Xilinx AXI Datamover IP */
Reg#(bit) rg_out_write_data_ddr_valid <- mkReg(0);
Reg#(Bit#(32)) rg_out_write_data_ddr_data <- mkReg(0);
Reg#(bit) rg_out_write_data_ddr_last <- mkReg(0);
Wire#(bit) wr_in_write_data_ddr_ready <- mkWire();

/* Wires of the Write Command interface of the Xilinx AXI Datamover IP */
Reg#(bit) rg_out_write_cmd_ddr_valid <- mkReg(0);
Wire#(bit) wr_in_write_cmd_ddr_ready <- mkWire();
Reg#(UInt#(32)) rg_out_ddr_write_addr <- mkReg(0);
Reg#(UInt#(23)) rg_out_ddr_write_length <- mkReg(0);

/* Wires of the Write Status interface of the Xilinx AXI Datamover IP */
Reg#(bit) drg_out_write_sts_ddr_ready <- mkDReg(1'b0);
Wire#(bit) wr_in_write_sts_ddr_valid <- mkWire();
Wire#(Bit#(8)) wr_in_write_sts_ddr_data <- mkWire();

/* Vector which indicates a request to the arbiter of the different methods. */
Vector#(4, Reg#(bit)) rg_ddr_read_req <- replicateM(mkReg(0));
Vector#(4, Reg#(bit)) rg_ddr_write_req <- replicateM(mkReg(0));

Vector#(4, Wire#(Bool)) dwr_ddr_start_write_req <-
	replicateM(mkDWire(False));

/*
Vectors which hold the info for read and write to the DDR (which is sent to the
command interfaces of the Xilinx AXI Datamover IP).
*/
Vector#(4, Reg#(Ddr_info)) rg_read_info <- replicateM(mkRegU());
Vector#(4, Reg#(Ddr_info)) rg_write_info <- replicateM(mkRegU());

// vector which holds the data to be sent to the DDR */
Vector#(4, Reg#(Bit#(32))) rg_in_write_data <- replicateM(mkReg(0));
Vector#(4, Reg#(bit)) rg_in_write_data_valid_toggle <-
	replicateM(mkReg(1'b0));
Vector#(4, Reg#(bit)) rg_write_data_write_toggle <-
	replicateM(mkReg(1'b1));

// data which has to be written if write command method is active
Wire#(Bit#(TMul#(64, 8))) dwr_cmd_write_data <- mkDWire(0);

// register which holds the data if a completion is requested from main memory
Reg#(Vector#(TDiv#(16, 4), Bit#(32))) arr_get_compl_data <- mkRegU();

/* register for ifc_nand_flash_model */
Reg#(bit) rg_out_busy <- mkReg(0);  // high active busy indicator
Reg#(bit) rg_in_enable <- mkReg(0);  // high active enable
Reg#(UInt#(21)) rg_write_count <- mkReg(0);  // internal counter

/* local declarations for ddr_write */
// indicates that the last block is sent
Reg#(Bool) rg_last_block <- mkReg(False);
// ID of the method which has currently the grant of the write arbiter
Reg#(UInt#(2)) rg_write_cur_id <- mkRegU();

Reg#(Bool) rg_write_data_done <- mkReg(True);
Reg#(Bool) rg_write_cmd_done <- mkReg(True);
Reg#(Bool) rg_write_sts_done <- mkReg(True);
Wire#(Bool) dwr_write_cmd_hold_req <- mkDWire(False);

/* local declarations for ddr_read */
Reg#(Bool) rg_read_busy <- mkReg(False);  // DDR read interface busy
Reg#(UInt#(TLog#(TDiv#(4096, 4)))) rg_read_count <- mkReg(0);
Reg#(UInt#(3)) rg_cur_read_id <- mkReg(4);

/* local declarations for put_cmd */
Reg#(Bool) rg_put_cmd_busy <- mkReg(False);
Reg#(Bit#(TMul#(64, 8))) rg_put_cmd_tmp <- mkRegU();
Reg#(UInt#(TLog#(TMul#(64, 8)))) rg_put_cmd_count <- mkReg(0);

Wire#(bit) dwr_out_ddr_start_nvm_read <- mkDWire(0);
Wire#(bit) dwr_out_ddr_nvm_ready <- mkDWire(1'b1);
Wire#(bit) dwr_out_ddr_nvm_write_start_ready <- mkDWire(0);

let lv_write_busy = !rg_write_data_done || !rg_write_cmd_done ||
	!rg_write_sts_done;

function UInt#(3) fn_rr_ddr_write_id();
	/* Get the id of the client which got the grant.

	One extra value (the highest) is added to express that there is no grant.
	*/
	// default to no grant (4 is higher than all used values)
	UInt#(3) lv_granted_id = 4;
	for (Integer i = 0; i < 4; i = i + 1)
		if (rr_ddr_write.clients[i].grant())
			lv_granted_id = fromInteger(i);
	return lv_granted_id;
endfunction

function UInt#(3) fn_rr_ddr_read_id();
	/* Get the id of the client which got the grant.

	One extra value (the highest) is added to express that there is no grant.
	*/
	// default to no grant (4 is higher than all used values)
	UInt#(3) lv_granted_id = 4;
	for (Integer i = 0; i < 4; i = i + 1)
		if (rr_ddr_read.clients[i].grant())
			lv_granted_id = fromInteger(i);
	return lv_granted_id;
endfunction

if (`DEBUG == 1) begin
	rule display_grant;
		$display("%d: ddr_connection: %d is granted to read", $stime(),
			fn_rr_ddr_read_id());
		$display("%d: ddr_connection: %d is granted to write", $stime(),
			fn_rr_ddr_write_id());
		$display("%d: write 2: grant: %d, req: %d, ready: %d, busy: %d",
			$stime(), rr_ddr_write.clients[2].grant(),
			rg_ddr_write_req[2], wr_in_write_data_ddr_ready, lv_write_busy);
		$display("%d: write 3: grant: %d, req: %d, ready: %d, busy: %d",
			$stime(), rr_ddr_write.clients[3].grant(),
			rg_ddr_write_req[3], wr_in_write_data_ddr_ready, lv_write_busy);
	endrule: display_grant

	rule display_wires;
		$display("%d: write 2: start: %d", $stime(),
			dwr_ddr_start_write_req[2]);
		$display("%d: write 3: start: %d", $stime(),
			dwr_ddr_start_write_req[3]);
		$display("%d: dwr_write_cmd_hold_req: %d", $stime(),
			dwr_write_cmd_hold_req);
	endrule: display_wires

	rule display;
		$display("%d: ddr_connection: rg_ddr_read_req[3] == %d",
			$stime(), rg_ddr_read_req[3]);
	endrule: display
	rule display_1;
			$display("%d: dwr_out_ddr_start_nvm_read == %d",
			$stime(), dwr_out_ddr_start_nvm_read);
	endrule: display_1
	rule display_2;
			$display("%d: dwr_out_ddr_nvm_ready == %d",
			$stime(), dwr_out_ddr_nvm_ready);
	endrule: display_2
end

/* Rules which hold the request of the read arbiter until it is granted and
while waiting for DDR Read Data interface to be valid. */
for (Integer i = 0; i < 4; i = i + 1) begin
	rule request_ddr_read (
			!rr_ddr_read.clients[i].grant() && rg_ddr_read_req[i] == 1'b1 ||
			rr_ddr_read.clients[i].grant() && rg_ddr_read_req[i] == 1'b1 &&
			wr_in_read_data_ddr_valid == 1'b0);
		if (`DEBUG == 1) $display("%d: ddr_connection: hold read request for %d",
			$stime(), i);
		rr_ddr_read.clients[i].request();
	endrule
end

/* Rules which hold the request of the write arbiter until it is granted and
while waiting for DDR Write Data interface to be ready.

One cycle is lost in each write because of lv_write_busy!  // TODO TODO
*/
for (Integer i = 0; i < 4; i = i + 1) begin
	rule request_ddr_write (dwr_ddr_start_write_req[i]);// || rg_ddr_write_req[i] == 1'b1 || rr_ddr_write.clients[i].grant() && dwr_write_cmd_hold_req);
		if (`DEBUG == 1) $display("%d: ddr_connection: hold write 0 request for %d",
			$stime(), i);
		rr_ddr_write.clients[i].request();
	endrule
	rule request_ddr_write_1 (rg_ddr_write_req[i] == 1'b1);
		if (`DEBUG == 1) $display("%d: ddr_connection: hold write 1 request for %d",
			$stime(), i);
		rr_ddr_write.clients[i].request();
	endrule
	rule request_ddr_write_2 (rr_ddr_write.clients[i].grant() &&
			dwr_write_cmd_hold_req);
		if (`DEBUG == 1) $display("%d: ddr_connection: hold write 2 request for %d",
			$stime(), i);
		rr_ddr_write.clients[i].request();
	endrule
end





/*
for (Integer i = 0; i < 4; i = i + 1) begin
	rule request_ddr_write (dwr_ddr_start_write_req[i]);
		rr_ddr_write.clients[i].request();
	endrule
	rule request_ddr_write_1 (rg_ddr_write_req[i] == 1'b1);
		rr_ddr_write.clients[i].request();
	endrule
	rule request_ddr_write_2 (rr_ddr_write.clients[i].grant() &&
			dwr_write_cmd_hold_req);
		rr_ddr_write.clients[i].request();
	endrule
end
*/

/*
for (Integer i = 0; i < 4; i = i + 1) begin
	rule request_ddr_write (
			dwr_ddr_start_write_req[i] ||
			rg_ddr_write_req[i] == 1'b1 ||
			rr_ddr_write.clients[i].grant() && dwr_write_cmd_hold_req
		);
		rr_ddr_write.clients[i].request();
	endrule
end
*/





/* Begin of DDR Write rules. */
for (Integer i = 0; i < 4; i = i + 1) begin
	rule ddr_write_1 if (fn_rr_ddr_write_id == fromInteger(i) &&
			lv_write_busy == False);
		/* Handle the access to the command and the data interface for write
		requests to the DDR */
		if (`DEBUG == 1) $display("%d: ddr_connection: ddr write_1 %d",
			$stime(), i);
		/* new transmission */
		//$display("lv_write_busy == False\n");
		// set up transmission
		rg_out_write_cmd_ddr_valid <= 1'b1;
		rg_out_ddr_write_addr <= rg_write_info[i].address;
		rg_out_ddr_write_length <= rg_write_info[i].length;
		rg_write_data_done <= False;
		rg_write_cmd_done <= False;
		rg_write_sts_done <= False;
	endrule: ddr_write_1


	rule ddr_write_2 if (fn_rr_ddr_write_id == fromInteger(i) &&
			lv_write_busy && rg_out_write_cmd_ddr_valid == 1'b1 &&
			!rg_write_cmd_done);
		if (`DEBUG == 1) $display("%d: ddr_connection: ddr write_2 %d",
			$stime(), i);
		if (wr_in_write_cmd_ddr_ready == 1'b1) begin  // TODO is timing ok?
			/* command interface for write is ready */
			rg_out_write_cmd_ddr_valid <= 1'b0;
			rg_write_cmd_done <= True;
		end else begin
			dwr_write_cmd_hold_req <= True;
		end
	endrule: ddr_write_2

	rule ddr_write_3 if (fn_rr_ddr_write_id == fromInteger(i) &&
			rg_last_block == False && lv_write_busy == True &&
			!rg_write_data_done);
		if (`DEBUG == 0) $display("%d: ddr_connection: ddr write_3 %d (%d - %d)",
			$stime(), i, rg_in_write_data_valid_toggle[i],
			rg_write_data_write_toggle[i]);
		if (
				wr_in_write_data_ddr_ready == 1'b1 &&
				rg_in_write_data_valid_toggle[i] ==
					rg_write_data_write_toggle[i]
			) begin
			/* ready to transmit */
			if (rg_ddr_write_req[i] == 1'b0) begin
				/* no more request, so it is the last block */
				//$display("Write last block\n");
				rg_last_block <= True;
				rg_out_write_data_ddr_last <= 1'b1;
			end else begin
				rg_out_write_data_ddr_last <= 1'b0;
			end
			// assign output signals
			if (`DEBUG == 1) $display("%d: ddr_connection: write data: 0x%x\n",
				$stime(), rg_in_write_data[i]);
			rg_out_write_data_ddr_data <= rg_in_write_data[i];
			rg_out_write_data_ddr_valid <= 1'b1;
			// set toggle register to be ready for the next transfer
			rg_write_data_write_toggle[i] <= ~rg_write_data_write_toggle[i];
		end else if (rg_in_write_data_valid_toggle[i] ==
				~rg_write_data_write_toggle[i]) begin
			rg_out_write_data_ddr_valid <= 1'b0;
		end
	endrule: ddr_write_3
end

rule ddr_write_4 if (lv_write_busy == True && rg_last_block == True &&
		!rg_write_data_done);
	if (`DEBUG == 1) $display("%d: ddr_connection: ddr write_4",
			$stime());
	if (wr_in_write_data_ddr_ready == 1'b1) begin
		/* transmission of last block completed */
		//$display("last block\n");
		rg_out_write_data_ddr_valid <= 1'b0;
		rg_last_block <= False;
		rg_write_data_done <= True;
	end
endrule: ddr_write_4


rule ddr_write_5 if (lv_write_busy == True);
	drg_out_write_sts_ddr_ready <= 1'b1;
	if (wr_in_write_sts_ddr_valid == 1'b1)
		rg_write_sts_done <= True;
endrule: ddr_write_5
/* End of DDR Write rules. */


rule put_cmd (wr_in_write_data_ddr_ready == 1'b1 && (
		(
			rg_ddr_write_req[2] == 1'b0 && 
			fn_rr_ddr_write_id != 2 &&
			!lv_write_busy
		) || (
			rg_ddr_write_req[2] == 1'b1 &&
			fn_rr_ddr_write_id == 2 &&
			lv_write_busy
		)));
	/* Do the transmission work if a command has to be writte to main memory. */
	if (rg_put_cmd_busy == True || dwr_cmd_write_data != 0) begin
		/* transmission to be done */
		if (`DEBUG == 1) $display("%d: ddr_connection: rg_put_cmd_count < 16: %d",
			$stime(), rg_put_cmd_count);
		if (rg_put_cmd_count == 0) begin
			/* first block to be transmitted */
			// data is taken directly taken from wire
			rg_in_write_data[2] <= dwr_cmd_write_data[31:0];
			// save the command to be transmitted for the next cycles
			rg_put_cmd_tmp <= dwr_cmd_write_data;
		end else begin
			// take data from saved blocks
			rg_in_write_data[2] <= rg_put_cmd_tmp[
				(rg_put_cmd_count + 1) * 32 - 1:rg_put_cmd_count * 32];
		end
		rg_in_write_data_valid_toggle[2] <= ~rg_in_write_data_valid_toggle[2];
		if (rg_put_cmd_count == 15) begin
			/* command transmission completed */
			if (`DEBUG == 1) $display("%d: ddr_connection: rg_put_cmd done: %d",
				$stime(), rg_put_cmd_count);
			rg_ddr_write_req[2] <= 1'b0;
			rg_put_cmd_busy <= False;
			rg_put_cmd_count <= 0;
		end else begin
			dwr_ddr_start_write_req[2] <= True;
			rg_ddr_write_req[2] <= 1'b1;
			rg_put_cmd_busy <= True;
			rg_put_cmd_count <= rg_put_cmd_count + 1;
		end
	end
endrule: put_cmd









rule ddr_read;
	/* Handle the access to the command and the data interface for read
	requests to the DDR */
	Bool lv_granted = False;
	if (rg_read_busy == False) begin
		/* wait for next transmission */
		if (fn_rr_ddr_read_id() < 4) begin
			/* new grant for transmission */
			$display("%d: ddr_connection: send read request 0x%x, 0x%x",
				$stime(), rg_read_info[fn_rr_ddr_read_id()].address,
				pack(rg_read_info[fn_rr_ddr_read_id()].address + 'h01000000));
			rg_cur_read_id <= fn_rr_ddr_read_id();
			rg_out_ddr_read_addr <= rg_read_info[fn_rr_ddr_read_id()].address;
			rg_out_ddr_read_length <= rg_read_info[fn_rr_ddr_read_id()].length;
			rg_out_read_cmd_ddr_valid <= 1'b1;
			rg_read_busy <= True;
		end
	end else begin
		if (rg_ddr_read_req[rg_cur_read_id] == 1'b0) begin
			/* transmission completed */
			rg_read_busy <= False;
		end
		if (wr_in_read_cmd_ddr_ready == 1'b1) begin  // TODO is timing ok?
			/* command interface for read is ready */
			//$display("%d: read info sent", $stime());
			rg_out_read_cmd_ddr_valid <= 1'b0;
		end
	end
endrule: ddr_read


rule read_data (
		wr_in_read_data_ddr_valid == 1'b1 &&
		rg_ddr_read_req[2] == 1'b1 &&
		fn_rr_ddr_read_id() == 2
	);
	/* Handle read of data from DDR if completion is requested from main memory. */
	dwr_out_read_data_ready <= 1'b1;  // acknowledge read
	if (`DEBUG == 1) $display("%d: ddr_connection: completion get 0x%x",
		$stime(), wr_in_read_data_ddr_data);
	arr_get_compl_data[rg_read_count] <= wr_in_read_data_ddr_data;
	if (wr_in_read_data_ddr_last == 1'b1) begin
		/* read finished */
		//$display("read last\n");
		rg_ddr_read_req[2] <= 1'b0;
		rg_read_count <= 0;
	end else begin
		/* get next dword */
		rr_ddr_read.clients[2].request();  // hold the request for the ddr
		rg_read_count <= rg_read_count + 1;
	end
	if (`DEBUG == 1) $display("%d: ddr_connection: completion read got 0x%x from main memory",
		$stime(), wr_in_read_data_ddr_data);
endrule: read_data

/*
rule display (rg_out_read_cmd_ddr_valid == 1'b1);
	$display("%d: ddr_connection: read info valid 0b%b", $stime, {
		4'b0,  // reserved
		4'b0,  // command tag; not used
		  // start address, 'h01000000 is offset for the DDR
		pack(rg_out_ddr_read_addr + 'h01000000),
		1'b0,  // DRE ReAlignment Request; not used
		1'b1,  // end of frame
		6'b0	,  // DRE Stream Alignment; not used
		1'b1,  // INCR access type
		pack(rg_out_ddr_read_length)  // bytes to transfer
	});
endrule
*/


rule ddr_nvm_ready (rg_ddr_read_req[3] == 1'b1);
		dwr_out_ddr_nvm_ready <= 1'b0;
endrule: ddr_nvm_ready

rule ddr_nvm_ready1 (dwr_out_ddr_start_nvm_read == 1'b1);
		dwr_out_ddr_nvm_ready <= 1'b0;
endrule: ddr_nvm_ready1


rule ddr_nvm_write_start_ready (
		rg_ddr_write_req[3] == 1'b0 &&
		fn_rr_ddr_write_id != 3 &&
		!lv_write_busy
	);
	dwr_out_ddr_nvm_write_start_ready <= 1'b1;
endrule: ddr_nvm_write_start_ready
/*
rule ddr_nvm_write_start_ready1 (
		!dwr_ddr_start_write_req[3] &&
		fn_rr_ddr_write_id != 3 &&
		!lv_write_busy
	);
	dwr_out_ddr_nvm_write_start_ready <= 1'b1;
endrule: ddr_nvm_write_start_ready1
*/

/*
TODO
The RAM has to be initialized to zero, to get the right phase tags.
TODO
*/

// vector which holds the use status of all PRPs
Vector#(`NO_PRPs, Reg#(Bool)) rg_prp_allocated <- replicateM(mkReg(False));


/* Main Memory Interface for the Testbench and the NVMe Controller. */
interface Ifc_main_memory_model_32 ifc_main_memory_model;
	method ActionValue#(UInt#(64)) _allocate_prp_();
		/* Allocate a Physical Region Page. */
		UInt#(TLog#(`NO_PRPs)) lv_prp_id = 0;

		// get the page address of the  next free page
		// count down to get the free PRP with the smallest number
		for (Integer i = `NO_PRPs - 1; i >= 0; i = i - 1) begin
			if (rg_prp_allocated[i] == False) begin
				lv_prp_id = fromInteger(i);
			end else if (i == 0 && lv_prp_id == 0) begin
				/* no free PRP has been found */
				// TODO better error handling
				$display("Error: All PRPs used. This is UNDEFINED!!!");
			end
		end

		// mark the page as used
		rg_prp_allocated[lv_prp_id] <= True;

		// return the PRP address consisting of base address and offset
		UInt#(TSub#(64, TAdd#(`MPS, 12))) lv_prp_base =
			'h10 + extend(lv_prp_id);  // see memory map
		UInt#(TAdd#(`MPS, 12)) offset = 0;
		let lv_prp_addr = unpack({pack(lv_prp_base), pack(offset)});
		return lv_prp_addr;
	endmethod: _allocate_prp_

	method Action _free_prp(UInt#(64) _prp_addr);
		/* Free the allocated PRP with the address '_prp_addr'. */
		let lv_prp_base = unpack(pack(_prp_addr)[63:`MPS + 12]);
		UInt#(64) lv_tmp = lv_prp_base - 'h10;  // see memory map
		UInt#(TLog#(`NO_PRPs)) lv_prp_id = truncate(lv_tmp);
		rg_prp_allocated[lv_prp_id] <= False;
	endmethod: _free_prp




	method Action _request_prp(UInt#(64) _prp_addr) if
			(rg_ddr_read_req[1] == 1'b0);
		/* Request the read of PRP.

		_prp_addr: Address of the PRP
		*/
		rg_read_info[1] <= Ddr_info {
			address: unpack(pack(_prp_addr)[31:0]),
			length: 4096
		};
		rr_ddr_read.clients[1].request();
		rg_ddr_read_req[1] <= 1'b1;
	endmethod: _request_prp

	method ActionValue#(Bit#(32)) get_prp_() if
			(wr_in_read_data_ddr_valid == 1'b1 && fn_rr_ddr_read_id() == 1);
		/* Return the PRP which has been requested by _request_prp. */
		dwr_out_read_data_ready <= 1'b1;
		if (wr_in_read_data_ddr_last == 1'b1)
			rg_ddr_read_req[1] <= 1'b0;
		else
			rr_ddr_read.clients[1].request();
		return wr_in_read_data_ddr_data;
	endmethod: get_prp_

	method Action _put_cmd(Nvm_command_type _cmd, UInt#(64) _sq_addr) if
			(wr_in_write_data_ddr_ready == 1'b1 && (
				rg_ddr_write_req[2] == 1'b0 && 
				fn_rr_ddr_write_id != 2 &&
				!lv_write_busy
			));
		/* Write a command to a submission queue.

		_sq_addr: Address of the submission queue
		*/
		if (`DEBUG == 0) $display("%d: ddr_connection: Write command with opcode %d to queue with address 0x%x",
			$stime(), _cmd.opcode, _sq_addr);
		rg_write_info[2] <= Ddr_info {
			address: unpack(pack(_sq_addr)[31:0]),
			length: 64
		};
		dwr_cmd_write_data <= pack(_cmd);
	endmethod: _put_cmd

	method Action _put_cmd_wait() if (rg_ddr_write_req[2] == 1'b0);
		/* Indicates a that the command is written to main memory. */
	endmethod: _put_cmd_wait

	method Action _request_compl(UInt#(64) _cq_addr) if
			(rg_ddr_read_req[2] == 0 && fn_rr_ddr_read_id != 2);
		/* Request the read of a completion from a completion queue.

		_cq_addr: The address of the completion queue
		*/
		if (`DEBUG == 0) $display("%d: ddr_connection: completion read request from 0x%x",
			$stime(), _cq_addr);
		rg_read_info[2] <= Ddr_info {
			address: unpack(pack(_cq_addr)[31:0]),
			length: 16
		};
		rr_ddr_read.clients[2].request();
		rg_ddr_read_req[2] <= 1'b1;
	endmethod: _request_compl

	method Completion_type get_compl_() if (rg_ddr_read_req[2] == 1'b0);
		/* Return the completion which has been requested by _request_compl. */
		return unpack(pack(arr_get_compl_data));
	endmethod: get_compl_




	method Action _nvm_request_data(UInt#(64) _address, UInt#(32) _length) if  // TODO why 10?
			(rg_ddr_read_req[3] == 1'b0);
		if (`DEBUG == 0) $display("%d: ddr_connection: NVM requests to read %d dwords from 0x%x",
			$stime(), _length, _address);
		// save transmission info
		rg_read_info[3] <= Ddr_info {
			address: truncate(_address),
			length: truncate(_length) * 4
		};
		// request read arbiter
		rr_ddr_read.clients[3].request();
		dwr_out_ddr_start_nvm_read <= 1'b1;
		rg_ddr_read_req[3] <= 1'b1;
	endmethod: _nvm_request_data

	method bit nvm_request_data_ready_();
		/* Generate ready signal for _nvm_request_data method. */
		return dwr_out_ddr_nvm_ready;
	endmethod: nvm_request_data_ready_

	method ActionValue#(Bit#(32)) _nvm_get_data_() if
			(fn_rr_ddr_read_id == 3 && wr_in_read_data_ddr_valid == 1'b1);
		if (`DEBUG == 1) $display("%d: ddr_connection: _nvm_get_data_: 0x%x (last: %d)",
			$stime(), wr_in_read_data_ddr_data, wr_in_read_data_ddr_last);
		dwr_out_read_data_ready <= 1'b1;
		if (wr_in_read_data_ddr_last == 1'b1)
			rg_ddr_read_req[3] <= 1'b0;
		else
			rr_ddr_read.clients[3].request();
		return wr_in_read_data_ddr_data;
	endmethod: _nvm_get_data_

	method Action _nvm_put_data(Bit#(32) _data, UInt#(64) _address,
			UInt#(32) _length) if (wr_in_write_data_ddr_ready == 1'b1 &&
			((
				rg_ddr_write_req[3] == 1'b0 && 
				fn_rr_ddr_write_id != 3 &&
				!lv_write_busy
			) || (
				rg_ddr_write_req[3] == 1'b1 &&
				fn_rr_ddr_write_id == 3 &&
				lv_write_busy
			)));
		if (`DEBUG == 0) $display("%d: ddr_connection: NVM requests to write 0x%x to 0x%x, write_count: %d, _length: %d",
			$stime(), _data, _address, rg_write_count, _length);
		if (rg_write_count == truncate(_length) - 1) begin
			/* transmission completed */
			rg_write_count <= 0;
			rg_ddr_write_req[3] <= 1'b0;
		end else begin
			/* transmission in progress */
			rg_write_info[3] <= Ddr_info {
				address: truncate(_address),
				length: truncate(_length) * 4
			};
			dwr_ddr_start_write_req[3] <= True;
			rg_ddr_write_req[3] <= 1'b1;
			rg_write_count <= rg_write_count + 1;
		end
		rg_in_write_data[3] <= _data;
		rg_in_write_data_valid_toggle[3] <= ~rg_in_write_data_valid_toggle[3];
	endmethod: _nvm_put_data

	method bit nvm_put_data_ready_();
		/* Generate ready signal for _nvm_put_data method. */
		if (wr_in_write_data_ddr_ready == 1'b1 &&
			((
				dwr_out_ddr_nvm_write_start_ready  == 1'b1
			) || (
				rg_ddr_write_req[3] == 1'b1 &&
				fn_rr_ddr_write_id == 3 &&
				lv_write_busy
			)))
			return 1'b1;
		else
			return 1'b0;
	endmethod: nvm_put_data_ready_
endinterface: ifc_main_memory_model



/* Nand Flash Interface for the NVMe Controller. */
interface Ifc_nand_flash_model_32 ifc_nand_flash_model;
	method Action _request_data(UInt#(64) _address, UInt#(11) _length) if
			(rg_in_enable == 1'b1 && rg_ddr_read_req[0] == 1'b0);
		/* Read request. */
		rg_read_info[0] <= Ddr_info {
			address: truncate(_address) * 4096,
			length: extend(_length) * 4096
		};
		rr_ddr_read.clients[0].request();
		rg_ddr_read_req[0] <= 1'b1;
		// response is generated in rl_bram_response
	endmethod

	method ActionValue#(Bit#(32)) _get_data_() if (rg_in_enable == 1'b1 &&
			fn_rr_ddr_read_id == 0 && wr_in_read_data_ddr_valid == 1'b1);
		/* Read register. */
		if (`DEBUG == 0) $display("%d: ddr_connection: NAND gets data: 0x%x",
			$stime(), wr_in_read_data_ddr_data);
		dwr_out_read_data_ready <= 1'b1;
		if (wr_in_read_data_ddr_last == 1'b1)
			rg_ddr_read_req[0] <= 1'b0;
		else
			rr_ddr_read.clients[0].request();
		return wr_in_read_data_ddr_data;
	endmethod: _get_data_

	method Action _write(UInt#(64) _address, Bit#(32) _data,
		UInt#(11) _length) if
			(rg_in_enable == 1'b1 && wr_in_write_data_ddr_ready == 1'b1 && (
				(
					rg_ddr_write_req[0] == 1'b0 && 
					fn_rr_ddr_write_id != 0 &&
					!lv_write_busy
				) || (
					rg_ddr_write_req[0] == 1'b1 &&
					fn_rr_ddr_write_id == 0 &&
					lv_write_busy
				)
			));
		/* Write register. */
		if (`DEBUG == 0) $display("%d: ddr_connection: NAND requests to write 0x%x to 0x%x, write_count: %d, _length: %d",
			$stime(), _data, _address, rg_write_count, _length);
		if (rg_write_count == 1024 * extend(_length) - 1) begin
			rg_write_count <= 0;
			rg_ddr_write_req[0] <= 1'b0;
		end else begin
			rg_write_info[0] <= Ddr_info {
				address: truncate(_address) * 4096,
				length: extend(_length) * 4096
			};
			dwr_ddr_start_write_req[0] <= True;
			rg_ddr_write_req[0] <= 1'b1;
			rg_write_count <= rg_write_count + 1;
		end
		rg_in_write_data[0] <= _data;
		rg_in_write_data_valid_toggle[0] <= ~rg_in_write_data_valid_toggle[0];
	endmethod: _write

	method Action _enable(bit _nand_ce_l);
		/* Enable or disable the NAND Controller.

		TODO can this be a timing problem because it is not enabled before the
		next cycle
		TODO what happens if it is disabled while busy?
		*/
		rg_in_enable <= ~_nand_ce_l;
	endmethod: _enable

	method bit interrupt_() if (rg_in_enable == 1'b1 &&
			fn_rr_ddr_read_id == 0);
		/* TODO */
		return 1'b1;
	endmethod: interrupt_

	method bit busy_() if (rg_in_enable == 1'b1);
		/* Return busy state of the NAND Controller. */
		return rg_out_busy;
	endmethod: busy_
endinterface: ifc_nand_flash_model






/* --- Interfaces for the connection to the Xilinx AXI Datamover IP. --- */
/* Interface to read payload data from Xilinx AXI Datamover IP. */
interface Ifc_read_data_ddr ifc_read_data_ddr;
	method bit ready_();
		return dwr_out_read_data_ready;
	endmethod: ready_

	method Action _valid(bit _is_valid);
		wr_in_read_data_ddr_valid <= _is_valid;
	endmethod: _valid

	method Action _data_in(Bit#(32) _data);
		wr_in_read_data_ddr_data <= _data;
	endmethod: _data_in

	method Action _last(bit _is_last);
		wr_in_read_data_ddr_last <= _is_last;
	endmethod: _last
endinterface: ifc_read_data_ddr


/* Interface to send read request to the Xilinx AXI Datamover IP. */
interface Ifc_read_cmd_ddr ifc_read_cmd_ddr;
	method bit valid_();
		return rg_out_read_cmd_ddr_valid;
	endmethod: valid_

	method Bit#(72) data_();
		return {
			4'b0,  // reserved
			4'b0,  // command tag; not used
			  // start address, 'h01000000 is base address of the DDR
			pack(rg_out_ddr_read_addr + 'h01000000),
			1'b0,  // DRE ReAlignment Request; not used
			1'b1,  // end of frame
			6'b0	,  // DRE Stream Alignment; not used
			1'b1,  // INCR access type
			pack(rg_out_ddr_read_length)  // bytes to transfer
		};
	endmethod: data_

	method Action _ready(bit _is_ready);
		wr_in_read_cmd_ddr_ready <= _is_ready;
	endmethod: _ready
endinterface: ifc_read_cmd_ddr


/* Interface to write payload data to the Xilinx AXI Datamover IP. */
interface Ifc_write_data_ddr ifc_write_data_ddr;
	method bit valid_();
		return rg_out_write_data_ddr_valid;
	endmethod: valid_

	method Bit#(32) data_out_();
		return rg_out_write_data_ddr_data;
	endmethod: data_out_

	method bit last_();
		return rg_out_write_data_ddr_last;
	endmethod: last_

	method Action _ready(bit _is_ready);
		wr_in_write_data_ddr_ready <= _is_ready;
	endmethod: _ready
endinterface: ifc_write_data_ddr


/* Interface to send write request to the Xilinx AXI Datamover IP. */
interface Ifc_write_cmd_ddr ifc_write_cmd_ddr;
	method bit valid_();
		return rg_out_write_cmd_ddr_valid;
	endmethod: valid_

	method Bit#(72) data_();
		return {
			4'b0,  // reserved
			4'b0,  // command tag; not used
			// start address, 0'h01000000 is base address of DDR
			pack(rg_out_ddr_write_addr + 'h01000000),
			1'b0,  // DRE ReAlignment Request; not used
			1'b1,  // end of frame
			6'b0,  // DRE Stream Alignment; not used
			1'b1,  // INCR access type
			pack(rg_out_ddr_write_length)  // bytes to transfer
		};
	endmethod: data_

	method Action _ready(bit _is_ready);
		wr_in_write_cmd_ddr_ready <= _is_ready;
	endmethod: _ready
endinterface: ifc_write_cmd_ddr

/* Interface to send receive write status from the Xilinx AXI Datamover IP. */
interface Ifc_write_sts_ddr ifc_write_sts_ddr;
	method bit ready_();
		return drg_out_write_sts_ddr_ready;
	endmethod: ready_

	method Action _valid(bit _is_valid);
		wr_in_write_sts_ddr_valid <= _is_valid;
	endmethod: _valid

	method Action _data_in(Bit#(8) _data);
		wr_in_write_sts_ddr_data <= _data;
	endmethod: _data_in
endinterface: ifc_write_sts_ddr


endmodule: mkDdr_connection
endpackage: ddr_connection
