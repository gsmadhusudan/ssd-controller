/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
module name: Memory Model
author name: Maximilian Singh
email id: maximilian.singh@student.kit.edu

This module provides the NAND Flash Model and the Main Memory Model for tests in
the simulator.  It simulates the interface of the Xilinx AXI Datamover IP and
stores the data in BRAMs.  In contrast to the Xilinx AXI Datamover IP read and
write do not operate simultaneously.
*/

`define DEBUG 1

package memory_model;

/* import bluespec libraries */
import BRAM::*;
import FIFOF::*;
import ConfigReg::*;

/* import user libraries */
import global_definitions::*;
import tb_nvm_controller::*;


(* synthesize *)
//(* always_ready *)
//(* always_enabled *)
module mkMemory_model(Empty);

Ifc_tb_for_nvm_controller lv_tb <- mkTb_for_nvm_controller;

Reg#(Bool) rg_outstanding_read <- mkReg(False);
Reg#(Bool) rg_outstanding_write <-mkReg(False);

Reg#(Datamover_cmd_type) rg_read_cmd <- mkRegU();
Reg#(Datamover_cmd_type) rg_write_cmd <- mkRegU();
Reg#(Bit#(TSub#(23, 2))) rg_read_dword_counter <- mkReg(0);
Reg#(Bit#(TSub#(23, 2))) rg_read_get_dword_counter <- mkConfigReg(0);
Reg#(Bit#(TSub#(23, 2))) rg_write_dword_counter <- mkReg(0);

Reg#(Maybe#(Bit#(32))) rg_read_data <- mkReg(Invalid);

BRAM_Configure lv_cfg_bram_memory = defaultValue;
lv_cfg_bram_memory.loadFormat = tagged Hex "init_memory_model.txt";
BRAM2Port#(Bit#(17), Bit#(32)) bram_memory <-
	mkBRAM2Server(lv_cfg_bram_memory);

FIFOF#(Bit#(32)) fifo_write <- mkSizedFIFOF(5);  // TODO size; FIFOF only for debugging

Wire#(bit) dwr_out_read_data_valid <- mkDWire(1'b0);
Wire#(bit) dwr_out_read_data_last <- mkDWire(1'b0);
Wire#(Bit#(32)) dwr_out_read_data_data <- mkDWire(0);
Wire#(bit) dwr_out_write_data_ready <- mkDWire(1'b0);
Wire#(bit) dwr_out_read_cmd_ready <- mkDWire(1'b0);
Wire#(bit) dwr_out_write_cmd_ready <- mkDWire(1'b0);

rule rl_read_idle (!rg_outstanding_read);
	if (`DEBUG == 1) $display("%d: memory_model: no read oustanding",
		$stime());
	dwr_out_read_cmd_ready <= 1'b1;
	rg_read_data <= tagged Invalid;
	if (lv_tb.ifc_read_cmd_ddr.valid_() == 1'b1) begin
		Datamover_cmd_type lv_read_cmd =
			unpack(lv_tb.ifc_read_cmd_ddr.data_());
		if (`DEBUG == 1) $display("%d: memory_model: address: 0x%x, length: %d, rg_dword_counter: %d",
			$stime(), lv_read_cmd.address, lv_read_cmd.length,
			rg_read_dword_counter);
		rg_read_cmd <= lv_read_cmd;
		rg_outstanding_read <= True;
	end
endrule: rl_read_idle

rule rl_write_idle (!rg_outstanding_write);
	if (`DEBUG == 1) $display("%d: memory_model: no write oustanding",
		$stime());
	dwr_out_write_cmd_ready <= 1'b1;
	if (lv_tb.ifc_write_cmd_ddr.valid_() == 1'b1) begin
		if (`DEBUG == 1) $display("%d: memory_model: received write request: 0b%b",
			$stime(), lv_tb.ifc_write_cmd_ddr.data_());
		rg_write_cmd <= unpack(lv_tb.ifc_write_cmd_ddr.data_());
		rg_outstanding_write <= True;
	end
endrule: rl_write_idle

rule rl_write;
	dwr_out_write_data_ready <= 1'b1;
	if (lv_tb.ifc_write_data_ddr.valid_() == 1'b1) begin
		if (`DEBUG == 1) $display("%d: memory_model: received 0x%x", $stime(),
			lv_tb.ifc_write_data_ddr.data_out_());
		fifo_write.enq(lv_tb.ifc_write_data_ddr.data_out_());
	end
endrule: rl_write

if (`DEBUG == 1) begin
	rule display;
		$display("%d: memory_model: rg_outstanding_write %d", $stime(), 
			rg_outstanding_write);
		if (!fifo_write.notEmpty())
			$display("%d: memory_model: fifo_write is empty", $stime());
		if (!fifo_write.notFull())
			$display("%d: memory_model: fifo_write is full", $stime());
	endrule
end

rule rl_write_bram (rg_outstanding_write == True);
	if (`DEBUG == 1) $display("%d: memory_model: write 0x%x to 0x%x (%d)",
		$stime(), fifo_write.first(), (rg_write_cmd.address - 'h01000000) / 4 +
			extend(rg_write_dword_counter), rg_write_dword_counter);
	bram_memory.portA.request.put(BRAMRequest{
		write: True,
		address: truncate((rg_write_cmd.address - 'h01000000) / 4 +
			extend(rg_write_dword_counter)),
		datain: fifo_write.first(),
		responseOnWrite: False
	});
	fifo_write.deq();
	if (rg_write_dword_counter == truncate(rg_write_cmd.length / 4) - 1) begin
		if (`DEBUG == 1) $display("%d: memory_model: last dword written.",
			$stime());
		rg_write_dword_counter <= 0;
		rg_outstanding_write <= False;
	end else begin
		rg_write_dword_counter <= rg_write_dword_counter + 1;
	end
endrule: rl_write_bram




rule rl_request_read (rg_outstanding_read &&
		rg_read_dword_counter < truncate(rg_read_cmd.length / 4) &&
		rg_read_get_dword_counter < truncate(rg_read_cmd.length / 4) - 1);
	if (`DEBUG == 1) $display("%d: memory_model: request data from 0x%x",
		$stime(), rg_read_cmd.address - 'h01000000 +
			extend(rg_read_dword_counter) * 4);
	bram_memory.portB.request.put(BRAMRequest{
		write: False,
		address: truncate((rg_read_cmd.address - 'h01000000) / 4 +
			extend(rg_read_dword_counter)),
		datain: ?,
		responseOnWrite: False
	});
	rg_read_dword_counter <= rg_read_dword_counter + 1;
endrule: rl_request_read

rule rl_get_read ((lv_tb.ifc_read_data_ddr.ready_() == 1'b1 ||
		rg_read_dword_counter == 1) &&  // first dword
		rg_outstanding_read &&
		rg_read_get_dword_counter < truncate(rg_read_cmd.length / 4));
	let lv_tmp <- bram_memory.portB.response.get();
	if (`DEBUG == 1) $display("%d: memory_model: get data: 0x%x (get counter: %d)",
		$stime(), lv_tmp, rg_read_get_dword_counter);
	rg_read_data <= tagged Valid lv_tmp;
	rg_read_get_dword_counter <= rg_read_get_dword_counter + 1;
endrule: rl_get_read

rule rl_send_read (isValid(rg_read_data) && rg_outstanding_read);  // TODO TODO timing ok?
	if (`DEBUG == 1) $display("%d: memory_model: read 0x%x (get counter: %d, length: %d)",
		$stime(), fromMaybe(?, rg_read_data), rg_read_get_dword_counter,
		rg_read_cmd.length);
	dwr_out_read_data_valid <= 1'b1;
	dwr_out_read_data_data <= fromMaybe(?, rg_read_data);
endrule: rl_send_read

rule rl_last_read (isValid(rg_read_data) &&
		rg_read_get_dword_counter == truncate(rg_read_cmd.length / 4) &&
		rg_outstanding_read);
	dwr_out_read_data_last <= 1'b1;  // TODO TODO timing? DWire?
endrule: rl_last_read

rule rl_finish_read (
		rg_outstanding_read &&
		rg_read_get_dword_counter == truncate(rg_read_cmd.length / 4)
	);
	if (lv_tb.ifc_read_data_ddr.ready_() == 1'b1) begin
		rg_read_dword_counter <= 0;
		rg_read_get_dword_counter <= 0;
		rg_outstanding_read <= False;
	end
endrule: rl_finish_read

rule rl_output;
	lv_tb.ifc_read_data_ddr._valid(dwr_out_read_data_valid);
	lv_tb.ifc_read_data_ddr._last(dwr_out_read_data_last);
	lv_tb.ifc_read_data_ddr._data_in(dwr_out_read_data_data);
	lv_tb.ifc_write_data_ddr._ready(dwr_out_write_data_ready);
	lv_tb.ifc_read_cmd_ddr._ready(dwr_out_read_cmd_ready);
	lv_tb.ifc_write_cmd_ddr._ready(dwr_out_write_cmd_ready);
	lv_tb._reg_in(0);
	/*
	As data is written in one cycle, the write sts is can be set to always valid
	for correct operation of ddr connection (see ddr_connection.bsv).
	*/
	lv_tb.ifc_write_sts_ddr._valid(1'b1);
	lv_tb.ifc_write_sts_ddr._data_in(0);
endrule: rl_output



endmodule: mkMemory_model

endpackage: memory_model
